# Predicate Logic Truth Tree

A Ruby library and script for validating sets of predicate logic formulas and checking them for consistency

Well-Formed Formulas
--------------------

A well-formed formula or 'wff' is an 'expression' that follows very strict rules.  An expression is any sequence of allowed characters; these are:

- predicate letters:        A, B, ... Z, A1, B1, Z1, ...
- variables:                u, v, w, x, y, z, u1, v1, w1, x1, y1, z1, ...
- names:                    a, b, c, d, a1, b1, c1, d1, ...  (t, t1, t2, ... are reserved names)
- opening parenthesis:      (
- closing parenthesis:      )
- quantifiers:              ∀, ∃
- connectives:              ~, ∨, &, →, ↔
- identity symbols:         =, ≠

A few other characters are allowed in input strings, but are not strictly part of the language.  These are: 

- whitespace
- alternate opening parenthesis: { [ 
- alternate closing parenthesis: } ]

An expression (an array of tokens) can be checked to see if it is a well-formed formula (wff).
Every wff must be one of the following sub-types: 

- an atomic sentence 
- a negation
- a conjunction
- a disjunction
- a conditional
- a biconditional
- a universal
- an existential
- an identity

Conjunctions, disjuctions, conditionals and biconditionals must be enclosed in parentheses (loosening this restriction and adding operator precedence rules is a todo item).  Each type of well-formed formula - except atomic sentences and negated identities, which are already fully decomposed - knows how to decompose itself into one or more collections of simpler well-formed formulas.  

In the context of a tree, each of these collections resulting from a decomposition will become a new leaf node, which when combined with its ancestor nodes, represents a 'branch' of the tree.  Some wffs are branching, others are not; the decomposition of an existential may result in any number of branches, depending on the names in the branch and the decomposition method used.  Currently only the ∃D2 method is used, which creates one new branch for every name already in the branch and one new name that doesn't exist in the branch.

Trees, Branches, Nodes and Elements
-----------------------------------

At any given point in the processing of a truth tree, the tree can be viewed simply as a collection of leaf nodes.  Each node contains a list of elements, each of which references a wff.  Each leaf node (except the root node containing the original set members) references a parent node.  In this way a "branch" can be traversed from a leaf node back up to the root node.  

Some tree elements may contain simple literals, others compound formulas.  Compound formulas are initially "pending", but become "complete" or non-pending once they have been (fully) decomposed.  Universals and Identies may require multiple decomposition steps before they are considered complete in the context of a given branch.  All other compound formulas require only a single decomposition step to become complete.  Tree elements that are complete are marked with a check on the tree diagram (see below).

A tree can be created in several ways: from a UTF-8 string, an array of symbols or an array of tokens.    

A branch (the traversal of a leaf node) is considered "closed" if it contains a pair of contradictory literals or a negated self-referential identity. Otherwise, the branch is "open".  An open branch is "complete" if all its Wffs have been fully decomposed.  A tree can be considered to be complete if either 

- all its branches are closed, in which case we can conclude that its set of wffs is inconsistent or 
- it contains a complete open branch, which represents a "Model" or set of assignments that make the set of wffs consistent.  If a branch is not closed or open, it is "pending".

In the latter case, usually a single model (or counter-model) will suffice when analyzing a set of sentences for consistency, so the interactive program (described below) stops if a model is found.  However, it is possible to continue searching for more models until no pending branches remain.


Formula Selection
-----------------

A given set of formulas can result in many different tree structures depending on the order in which the formulas are decomposed.  Some of these trees may be much simpler than others.  It's desirable to have a strategy for selecting formulas that results in the simplest possible tree.  For example, a wff with a non-branching decomposition would generally be selected before one with a branching decomposition.

In an attempt to optimize the selection sequence, the formula selector included in this library can perform a "test decomposition" of a wff into a given branch and compare the counts of pending branches before and after the decomposition.  This is done by adding some temporary leaf nodes to the tree, checking the count of pending branches and then restoring the tree to its original state.

Currently the rules applied by the formula selector are pretty simplistic: 

- Try to find a non-branching formula, 
- Try to find a branching formula whose test decomposition does not result in an increase in the number of pending branches (this includes existentials via ∃D2)
- Try to find a universal whose test decomposition will result in a net reduction of pending branches
- Try to find any universal which hasn't been decomposed in favor of all names in the branch
- Try to find an identity which has pending substitutions.

Output
------

The diagram provides simple text-based output representing a tree.  Each decomposition step is displayed as one or more numbered lines in the diagram.  After the line number, each formula assigned to the line is listed.  

An annotation explaining the origin of the formulas on the line is provided at the far right.  'SM' indicates that the formula on the line is an original set member.  The other annotations specify one or more line numbers followed by a decomposition step abbreviation.  For example, the annotation '2 ~&D' indicates that the formulas on the current line were obtained by decomposing a negated conjunction on line 2.

Each formula is preceded by its node id and the id of its parent node in parentheses, except for the root node which has no parent.  This notation, while not as visually appealing as a graph with lines connecting the nodes, does make it possible to trace any branch from a leaf up to the root node. 

After all the lines have been displayed, some additional information is provided depending on the state of the tree and its various leaf nodes.

If any models have been found, the output includes predicate assignments and the set of names constituting the universe.

Command Line Usage
------------------

To run interactively from the command prompt with Ruby 1.9 or above:

    $ ruby ttree.rb data/samples/S85

There are currently four command options:

- r(un) [to line]          asks the program to process the tree until all branches are closed, a (new) model is found or until the optional [to line] line number is reached
- f(ormula) <line> [node]  asks the program to process the formula on the specified line.  If more than one formula appears on the line, the node number must also be specified (e.g. f 3 1 selects line 3, node 1).  If the formula is a universal, an existential or an identity, you will be prompted for additional information (not implemented yet...)
- a(gain)                  asks the program to start a fresh tree with the set of formulas that were specified at the command line.
- q(uit)                   quits the program

Sample Command Line Session
---------------------------

    dow@dow-laptop ~/ruby/tree_of_truth $ ruby ttree.rb data/samples/S85 
    got 4 lines

    1.       1: ((P & Q) → ((R ∨ S) & ~(R & S)))      SM
                                               
    2.       1: ((R & Q) → S)                         SM
                            
    3.       1: (S → (((R & Q) ∨ (~R & ~Q)) ∨ ~P))    SM
                                                 
    4.       1: ~(P → ~Q)                             SM
                        
    Tree has 1 pending leaves: 1
    Please select an option:
    r(un) [to line], f(ormula) <line> [node], a(gain), q(uit)

    f 3

    1.       1: ((P & Q) → ((R ∨ S) & ~(R & S)))      SM
                                                   
    2.       1: ((R & Q) → S)                         SM
                                
    3.       1: (S → (((R & Q) ∨ (~R & ~Q)) ∨ ~P)) ✓            SM
                                                       
    4.       1: ~(P → ~Q)                             SM
                            
    5.       2(1): ~S ✓      3(1): (((R & Q) ∨ (~R & ~Q)) ∨ ~P)           3 →D
                                                                  
    Tree has 2 pending leaves: 2, 3
    r(un) [to line], f(ormula) <line> [node], a(gain), q(uit)
    f 4

    1.       1: ((P & Q) → ((R ∨ S) & ~(R & S)))      SM
                                                   
    2.       1: ((R & Q) → S)                         SM
                                
    3.       1: (S → (((R & Q) ∨ (~R & ~Q)) ∨ ~P)) ✓            SM
                                                       
    4.       1: ~(P → ~Q) ✓                           SM
                              
    5.       2(1): ~S ✓      3(1): (((R & Q) ∨ (~R & ~Q)) ∨ ~P)           3 →D
                                                                  
    6.       4(2): P ✓      5(3): P ✓                 4 ~→D
                                        
    7.       4(2): Q ✓      5(3): Q ✓                 4 ~→D
                                        
    Tree has 2 pending leaves: 4, 5
    r(un) [to line], f(ormula) <line> [node], a(gain), q(uit)


Tests
-----

This library is supported by an extensive set of tests.  Many of the high-level tests are sequents or countermodel examples from "Logic Primer" by Allen & Hand.  Out of hundreds of high-level tests, only a couple currently fail to converge.  Any of the high level tests can be easily converted into an input file and processed interactively.

Todo
----

- Work out the trees for the non-converging sets by hand and see if there's a way to get them to converge.  The non-converging sets are:
    - 4.3.1 ix (modified -- conclusion in book is not a wff): ∀x∃yFxy, ∃x~∀yGyx, (∃x∃yFxy ↔ ∃x∃y(Gyx & ~Gxy)), ~∃x∃y(Gxy ∨ Gyx)
    - 4.3.2 xviii:  ∀x∀y∀z((Fxy&Fyz)→Fxz), ∀x∃y(Fxy→Fyx), ~(∀x∃yFxy → ∀xFxx)
- Add some parentheses-dropping conventions and operator precedence rules on the input formulas.
- Improve the auto-generated names so that letters like a, b, c can be introduced in many cases, instead of t1, t2, etc...
- Improve the output, adding name extensions
- Consider using a graphical display of some sort, e.g. the Ruby Graph Library (RGL)
- Change the formula selection algorithm, so that when testing a wff, highest priority is given to a wff that results in an open complete branch -- even if it results in a net increase in the number of pending branches.
- Consider adding some sort of module to handle infinite models, such as those defining relations on the set of natural numbers.
- Consider adding free logic decomposition rules.
- Consider expanding names to functions of names.

Thanks
------

- Thanks to Professor David Weber of PSU for his excellent instruction on formal logic.

