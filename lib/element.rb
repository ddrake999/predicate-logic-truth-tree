# encoding: UTF-8

module TruthTree

  class Element

    attr_accessor :complete
    attr_reader :node, :wff, :parent_decomposition, :decomposition,
      :line, :annotation, :names_applied, :names_applied_backup

    # @names_applied applies only to universals.  It's a list of names for which the wff has been instantiated.  
    # A universal must remain 'pending' until an instantiation has been made in favor of all names in the branch.  
    def initialize(node, wff, parent_decomposition, line)
      @decomposition = nil
      @node = node
      @wff = wff
      @parent_decomposition = parent_decomposition
      @line = line
      @step = parent_decomposition.step
      @annotation = parent_decomposition.annotation
      @eligible_for_substitutions = (wff.literal? || wff.is_a?(Identity)) && @step != :identity
      @complete = wff.literal?
      @names_applied_backup = []
      @names_applied = []
      @simple_nonbranching_decomp = case @wff
      when Universal, Identity, Existential, AtomicSentence then false
      else 
        @decomposition = get_decomposition(@wff)
        @decomposition.wffs.size < 2
      end
    end

    def to_s
      @wff.to_s
    end

    def simple_nonbranching_decomp?
      @simple_nonbranching_decomp
    end

    # args is a hash like {name: a_name} for universals or 
    # {element: an_element, new_wff: a_new_wff } for identities
    def decompose(args={}) 
      @decomposition = get_decomposition(@wff, args)
      @decomposition.ref_lines = [@line]
      if @wff.is_a? Identity
        elem = args[:element]
        @decomposition.ref_lines << elem.line 
      elsif @wff.is_a? Universal
        args[:leaf_node].universal_instantiations[@wff] << args[:name]
      end
      @decomposition
    end

    def get_decomposition(wff, args={})
      case @wff
      when Existential
        raise "Existentials need a leaf node" unless args[:leaf_node]
        # just using ∃D2 for now.  The leaf node is specified in Tree :add_decomposition
        @wff.decompose(args[:leaf_node].names + [args[:leaf_node].new_name])
      when Universal
        raise "Universals need a name and a leaf node" unless args[:name] && args[:leaf_node]
        @wff.decompose( args[:name] )
      when Identity
        raise "An element and a new wff must be specified for an identity" unless args[:element] && args[:new_wff]
        @wff.decompose(args[:element].wff, args[:new_wff])
      else
        @decomposition ||= @wff.decompose
      end
    end

    def pending?(for_node=@node)
      case @wff
      when Universal
        for_node.universal_pending?(@wff)
      when Identity
        elems = for_node.eligible_elements_for_identity( @wff )
        !! elems.detect do |e| 
          for_node.first_wff_not_in_node_elements(e.name_substitutions(@wff))
        end
      else
        ! @complete
      end
    end

    def symbols
      @wff.symbols
    end

    def eligible_for_substitutions?
      @eligible_for_substitutions
    end

    # Given an identity, and when this BranchWff's Wff is eligible for substitutions, get all possible wffs
    # that could be formed by making the identity substitution on this BranchWff's Wff.
    def name_substitutions(identity)
      raise "Expecting an Identity, got a #{identity.class}" unless identity.is_a?(Identity)
      raise "Can't substitute into self" if self.equal?(identity)
      raise "Not eligible_for_substitutions" unless @eligible_for_substitutions
      name_syms = wff.name_syms
      subs = self.class.symbol_substitutions_for(name_syms, identity.left_name.symbol, identity.right_name.symbol)
      subs = subs.delete_if { |s| s == name_syms }
      if wff.is_a?(Identity) || Wff.negated_identity?(wff.tokens)
        wffs = subs.map { |s| Wff.from_symbols([s[0], wff.tokens[1].symbol, s[1]]) }
      elsif wff.is_a?(Negation)
        wffs = subs.map { |s| Wff.from_symbols([:tilde, wff.tokens[1].symbol] + s)}
      else
        wffs = subs.map { |s| Wff.from_symbols([wff.tokens[0].symbol] + s) }
      end
    end

    # Return a list all symbol substitutions for a list of symbols.  The original list is returned as a member.
    def self.symbol_substitutions_for(symbols, find, replacement)
      results = []
      if symbols.size == 1
        results << symbols[0..-1]
        results << [replacement] if symbols[0] == find
      elsif symbols.size > 1
        s1 = symbols[0]
        substitutions = self.symbol_substitutions_for(symbols[1..-1], find, replacement)
        results += substitutions.map { |ar| [s1] + ar }
        results += substitutions.map { |ar| [replacement] + ar } if s1 == find
      end
      results
    end

    def univ_or_ident
      wff.is_a?(Universal) || wff.is_a?(Identity)
    end

    def belongs_only_to_closed_leaves?
      ! @node.tree.pending_leaves.detect { |n| n.contains_element?(self) }
    end


  end


end
