# encoding: UTF-8

module TruthTree

  class Tree

    attr_accessor :leaves, :new_leaves, :last_line, :last_node_id
    attr_reader :formula_selector, :diagram

    def initialize
      @leaves = []
      @new_leaves = []
      @last_line = 0
      @last_node_id = 0
      @formula_selector = FormulaSelector.new(self)
      @diagram = Diagram.new(self)
      @time_selecting = 0
      @time_processing = 0
    end

    def next_node_id
      @last_node_id += 1
    end

    def self.from_wffs(wffs)
      tree = self.new
      node = Node.new( tree )
      node.set_elements( Decomposition.new( :set_member, [wffs] ), 0 )
      tree.leaves << node
      tree.last_line = wffs.count
      tree
    end

    def self.from_symbols(symbols)
      from_wffs( symbols.map { |s| Wff.from_symbols(s) } )
    end

    def self.from_lines(lines)
      from_wffs( lines.map { |l| Wff.from_string(l) } )
    end

    # need to consider new leaves here for test decompositions.
    def pending_leaves
      (@leaves + @new_leaves).select { |leaf| leaf.state == :pending } - @new_leaves.map { |node| node.parent }.uniq
    end

    def open_complete_leaves
      @leaves.select { |leaf| leaf.state == :open_complete }
    end

    def closed_leaves
      @leaves.select { |leaf| leaf.state == :closed }
    end


    #----------------------
    # FORMULA DECOMPOSITION
    #----------------------
    def decompose(element, args={})
      raise "Element has already been decomposed (not pending)" if element.complete
      set_new_leaves(element, args)
      begin
        return_value = yield(self) if block_given?
      ensure
        if args && args[:test]
          restore_original_state(element, args)
        else
          finalize_decomposition(element)
        end
        @new_leaves = []  
      end
      return_value
    end

    def set_new_leaves(element, args={})
      @new_leaves = []
      @original_name_indices = []
      if element.univ_or_ident
        raise "Invalid leaf node specified for element" unless args[:leaf_node].contains_element?(element)
        @original_name_indices << args[:leaf_node].last_new_name_idx
        @new_leaves += add_decomposition(args[:leaf_node], element, args)
      else
        @original_nodes = pending_leaves.select { |n| n.contains_element?(element) }
        raise "Element belongs to only closed leaves" if @original_nodes.empty?
        @original_name_indices = @original_nodes.map { |n| n.last_new_name_idx }
        element.complete = true
        @original_nodes.each do |n| 
          @new_leaves += add_decomposition(n, element, args)
        end
      end
    end

      def add_decomposition(parent_node, element, args={})
        args[:leaf_node] = parent_node if element.wff.is_a?(Existential)
        decomposition = element.decompose(args)
        nodes = decomposition.wffs.map.with_index do |wffs, i| 
          node = Node.new(self, parent_node).set_elements(decomposition, i)
        end
        nodes
      end

    def restore_original_state(element, args={})
      @last_node_id -= @new_leaves.count
      leaf_node = args[:leaf_node]
      if element.univ_or_ident
        restore_state_for_univ_or_ident(element, leaf_node, args)
      else
        restore_state_for_others
      end
      element.complete = false
    end

      def restore_state_for_univ_or_ident(element, leaf_node, args={})
        leaf_node.last_new_name_idx = @original_name_indices[0]
        leaf_node.state = :pending
        if element.wff.is_a?(Universal)
          leaf_node.universal_instantiations[element.wff].delete(args[:name])
        end
      end

      def restore_state_for_others
        @original_nodes.each.with_index do |node, i| 
          node.state = :pending 
          node.last_new_name_idx = @original_name_indices[i]
        end
      end

    def finalize_decomposition(element)
      @last_line += element.decomposition.lines
      @new_leaves.each do |l|
        @leaves.insert(@leaves.index(l.parent), l)
      end
      @new_leaves.each do |l|
        @leaves.delete(l.parent)
      end
    end


    #----------------
    # TREE PROCESSING
    #----------------
    def process_until_closed_or_model_found(inargs={})
      @time_selecting = 0
      @time_processing = 0
      model_found = false
      open_complete_count = open_complete_leaves.count 
      while pending_leaves.count > 0 && ! model_found
        break if inargs[:max_lines] && last_line >= inargs[:max_lines] 

        started_at = Time.now
        elem, args = @formula_selector.get_next
        @time_selecting += Time.now - started_at

        raise "No formula to decompose with #{pending_leaves.count} pending leaves. \
          Pending formula is #{pending_leaves.first.elements}" unless elem

        started_at = Time.now
        decompose(elem, args)
        @time_processing += Time.now - started_at

        model_found = (open_complete_leaves.count > open_complete_count)
      end
    end

    def run(args={})
      process_until_closed_or_model_found args
      display
    end

    def display
      diagram.render
      # puts
      # puts '%.3f ' % @time_selecting + " selecting"
      # puts '%.3f ' % @time_processing + " processing"
    end


    #----------------
    # DISPLAY HELPERS
    #----------------
    def elements_for_line(line)
      elements = []
      leaves.each do |node|
        if (elem = node.element_for_line(line) ) 
          elements << elem 
        end
      end
      elements.uniq
    end

    def incomplete_elements_for_line(line)
      elements_for_line(line).select { |elem| ! elem.complete }
    end



  end


end