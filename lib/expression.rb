# encoding: UTF-8

module TruthTree

  # an expression is a container of an array of tokens
  class Expression
    attr_reader :tokens

    def initialize(tokens)
      @tokens = tokens
    end

    def self.from_string(string)
      self.new Token.tokenize(string)
    end

    # get an array of tokens from an array of symbols
    def self.get_tokens( symbols )
      return symbols.map { |s| Token.from_sym(s) }
    end

    def self.from_symbols( symbols )
      self.new( get_tokens(symbols) )
    end

    # TODO: clean up with nice regexp
    def to_s
      str = @tokens.map.with_index { |t,i| pad_char(tokens,i) }.join('').gsub('  ',' ').gsub('~ ','~').gsub(' )', ')').gsub('( ', '(').strip
    end

    def pad_char(tokens, i)
      t = tokens[i]
      if t.symbol == :tilde then "~"
      elsif t.is_a?(Connective) then " #{t} "
      elsif t.symbol == :paren_open then " ("
      elsif t.symbol == :paren_close then ") "
      # ∀xx=a is hard to read. insert a space to get ∀x x=a 
      elsif ( t.is_a?(Name) || t.is_a?(Variable) ) && tokens[i+1] && [:eq, :ne].include?(tokens[i+1].symbol) then " #{t}"
      else t.to_s
      end
    end

    # return an array of symbols for the expression
    def symbols
      @tokens.map { |t| t.symbol }
    end

    # an array of symbols representing the names in the expression
    def unique_name_syms
      name_syms.uniq
    end

    # an array of symbols representing the names in the expression
    def name_syms
      @tokens.select { |t| t.is_a? Name }.map { |t| t.symbol }
    end

    # an array of indices for the given symbol or token
    def symbol_indices(symbol)
      s = symbol.is_a?(Token) ? symbol.symbol : symbol
      @tokens.each_index.select { |i| @tokens[i].symbol == s }
    end

    # expressions are equal if they have the same tokens in the same order
    def == (other)
      tokens.count == other.tokens.count &&
      ! tokens.each_index.detect { |i| tokens[i].symbol != other.tokens[i].symbol }
    end

    def self.parens?(tokens)
      tokens.first.symbol == :paren_open && tokens.last.symbol == :paren_close
    end


  end


end