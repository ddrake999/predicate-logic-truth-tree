# encoding: UTF-8

module TruthTree
  class Diagram
    attr_accessor :tree, :lines
    def initialize(tree)
      @tree = tree
      @lines = []
    end

    def build
      @lines = []
      get_tree_lines
      if @tree.open_complete_leaves.count > 0
        @lines << "\nModel(s) found\n" 
        get_models 
      end
    end

    def get_tree_lines
      (1..@tree.last_line).each do |i|
        line = "#{i}.".ljust(6)
        subline = " " * 6
        annotation = ''
        @tree.elements_for_line(i).each do |elem|
          s = "#{elem.node.branch_lbl}: #{elem.wff}"
          s << " (closes)" if elem == elem.node.closing_element
          if elem.complete
            s << " ✓" 
          elsif elem.belongs_only_to_closed_leaves?
            s << " ✓(x)" 
          end
          line << self.class.pad(s)
          subline << subline_char(elem).center( self.class.padded_length(s) )
          annotation = elem.annotation
        end
        # pad the right side of the string so the annotations line up (mostly)
        line = ( line.length < 50 ? line.ljust(50) : line.ljust((line.length/10 + 1) * 10) ) << annotation
        @lines << line << subline
      end
    end

    def get_models
      @tree.open_complete_leaves.each do |node|
        line = "Node #{node.id}: "
        line  += "Universe: {" + node.name_syms.map { |n| n.to_s }.join(', ') + "}   "
        line += "Predicate assignments: " + node.non_identity_literals.map { |elem| elem.to_s }.uniq.join(", ")
        @lines << line
      end
    end

    def render
      build
      puts
      puts @lines.join("\n")
    end

    def self.padded_length(s)
      pad_ct = 3
      s.length + pad_ct * 2
    end

    def self.pad(s)
      s.center( padded_length(s) )
    end

    def subline_char(element)
      node = element.node
      if node.state == :closed && node.elements.last == element then 'X'
      elsif node.state == :open_complete && node.elements.last == element then 'O'
      else ' '
      end   
    end
  end
end