# encoding: UTF-8

module TruthTree

  # The set of possible decompositions for a wff.
  DECOMPS = {
    set_member:           "SM",
    ampersand:            "&D",
    wedge:                "∨D",
    arrow:                "→D",
    double_arrow:         "↔D",
    forall:               "∀",
    exists:               "∃D",
    neg_ampersand:        "~&D",
    neg_wedge:            "~∨D",
    neg_arrow:            "~→D",
    neg_double_arrow:     "~↔D",
    neg_forall:           "~∀D",
    neg_exists:           "~∃D",
    double_neg:           "~~",
    identity:             "=D",
  }

  # --------------------------------------------------------------------------
  # Decomposition class, represents a decomposition of a wff into a truth tree
  # --------------------------------------------------------------------------
  class Decomposition

    attr_accessor :ref_lines 

    attr_reader :step, :wffs
    def initialize(step, wffs)
      # A DECOMP_STEP
      @step = step 

      # A nested array of child wffs produced by the decomposition, 
      # where each sub-array represents the expressions to be added to one leaf node (branch)
      @wffs = wffs 
      
      # an array with reference line numbers.  Wffs don't know or care about this.  It's set by the element. 
      # (an identity decomp references both the identity and the formula being substituted into)
      @ref_lines = []
    end

    def branch_ct
      @wffs.count { |w| w.size > 0 }
    end

    def lines
      @wffs.inject(0) { |max, w| w.size > max ? w.size : max } || 0
    end

    def annotation
      (@ref_lines.join(',') + ' ' + DECOMPS[@step]).strip
    end


  end
end