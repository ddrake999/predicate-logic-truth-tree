# encoding: UTF-8

module TruthTree

  # a negation of a wff
  class Negation < Wff

    def has_simple_non_branching_decomp?
      raise "A literal has no decomposition" if @is_literal
      return [Negation, Disjunction, Conditional].include?(child.class)
    end


    def child
      @child ||= @is_literal ? nil : Wff.from_tokens( @tokens[1..-1] )
    end


    def decompose
      return Decomposition.new(nil, [[]]) if @is_literal
      case child
      when AtomicSentence
        Decomposition.new(nil, [[]])
      when Negation
        double_neg_decomp child 
      when Conjunction
        neg_conjunction_decomp child
      when Disjunction
        neg_disjunction_decomp child
      when Conditional
        neg_conditional_decomp child
      when Biconditional
        neg_biconditional_decomp child
      when Universal
        neg_universal_decomp child
      when Existential
        neg_existential_decomp child
      else
        raise "Unexpected type #{child.class}"
      end
    end

    def double_neg_decomp(child)
      if Wff.negated_identity?(child.tokens)
        wff = Wff.from_tokens( [child.tokens[0], Token.from_sym(:eq), child.tokens[2] ] ) 
      else
        wff = Wff.from_tokens( child.tokens[1..-1] )
      end
      Decomposition.new(:double_neg, [[wff]])
    end

    def neg_disjunction_decomp(child)
      Decomposition.new(:neg_wedge, [get_sub_wffs(child)]) 
    end

    def neg_conjunction_decomp(child)
      subs = get_sub_wffs(child)
      if subs[0] == subs[1]
        wffs = [[subs[0]]]
      else
        wffs = subs.map { |s| [s] }
      end
      Decomposition.new( :neg_ampersand, wffs ) 
    end

    def get_sub_wffs(child)
      child.subs.map { |sub| sub.negate }
    end

    def neg_conditional_decomp(child)
      antecedent, consequent = child.subs
      wffs = [ [antecedent, consequent.negate ] ]
      Decomposition.new(:neg_arrow, wffs)
    end

    def neg_biconditional_decomp(child)
      sub1, sub2 = child.subs
      br1 = [sub1.negate, sub2]
      br2 = [sub1, sub2.negate]
      wffs = (sub1 == sub2 ? [br1] : [br1, br2])
      Decomposition.new(:neg_double_arrow, wffs)
    end

    def neg_universal_decomp(child)
      Decomposition.new(:neg_forall, get_exchange_wffs(child, :exists))
    end
    
    def neg_existential_decomp(child)
      Decomposition.new(:neg_exists, get_exchange_wffs(child, :forall))
    end
    
    def get_exchange_wffs(child, new_symbol)
      lpart = [ Token.from_sym(new_symbol), child.tokens[1] ]
      rpart = child.tokens[2..-1]

      if Wff.wff?(rpart)
        rpart = Wff.from_tokens(rpart).negate.tokens
      elsif open_identity?(rpart)
        rpart = [ rpart[0], Token.from_sym(rpart[1].symbol == :eq ? :ne : :eq), rpart[2] ]
      else
        rpart = [ Token.from_sym(:tilde) ] + rpart
      end
      wffs = [[ Wff.from_tokens( lpart + rpart ) ]]
    end

    def open_identity?(tokens)
      tokens.size == 3 && (tokens[0].is_a?(Name) || tokens[0].is_a?(Variable)) &&
                          (tokens[2].is_a?(Name) || tokens[2].is_a?(Variable))
    end


  end

end