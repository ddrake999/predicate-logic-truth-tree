# encoding: UTF-8

module TruthTree

  # an n-place predicate letter followed by n names (n >= 0) or 
  # an expression of the form alpha=beta where alpha and beta are names
  class AtomicSentence < Wff
    def decompose
      Decomposition.new( nil, [[]] )
    end

  end

end