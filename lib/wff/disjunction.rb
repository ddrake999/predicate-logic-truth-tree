# encoding: UTF-8

module TruthTree


  # a disjunction of two wffs
  class Disjunction < Wff
    def decompose
      if subs[0] == subs[1]
        wffs = [[subs[0]]]
      else
        wffs = subs.map { |s| [s] }
      end
      Decomposition.new(:wedge, wffs)
    end
  end

end