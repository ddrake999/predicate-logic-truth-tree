# encoding: UTF-8

module TruthTree


  # an existential: the result of replacing at least one occurrence of a name in a wff 
  #   by a new variable :alpha (not in the wff) and prefixing by :exists :alpha
  class Existential < Wff
    def decompose(names)
      # one new sub-branch is created for each name in the parent branch plus one more with a new name.
      variable = @tokens[1]
      wffs = names.map { |n| [ Wff.from_tokens(Wff.replacement(@tokens[2..-1], variable, n)) ] }
      Decomposition.new(:exists, wffs)
    end
  end

end