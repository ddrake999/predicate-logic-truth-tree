# encoding: UTF-8

module TruthTree


  # a bi-conditional of two wffs - Decompose this into a disjunction of two conjunctions.
  class Biconditional < Wff
    def decompose
      sub1, sub2 = subs
      br1 = [ sub1, sub2 ]
      br2 = [ sub1.negate, sub2.negate ]
      wffs = (sub1 == sub2.negate ? [br1] : [br1, br2])
      Decomposition.new(:double_arrow, wffs)
    end
  end

end