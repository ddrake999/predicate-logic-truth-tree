# encoding: UTF-8

module TruthTree

  # a universal: the result of replacing at least one occurrence of a name in a wff 
  #   by a new variable :alpha (not in the wff) and prefixing by :forall :alpha
  class Universal < Wff
    def decompose(name)
      variable = @tokens[1]
      wffs = [[ Wff.from_tokens(Wff.replacement(@tokens[2..-1], variable, name)) ]]
      Decomposition.new(:forall, wffs)
    end
  end


end