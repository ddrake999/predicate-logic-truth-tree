# encoding: UTF-8

module TruthTree


  # a conjunction of two wffs
  class Conjunction < Wff
    def decompose
      Decomposition.new(:ampersand, [subs])
    end
  end

end