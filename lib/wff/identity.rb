# encoding: UTF-8

module TruthTree

  # an Identity is a special kind of atomic sentence
  class Identity < AtomicSentence
    def left_name
      tokens[0]
    end

    def right_name
      tokens[2]
    end

    def tautologous?
      left_name == right_name
    end

    def decompose(wff, new_wff)
      raise "It's pointless to decompose a tautologous identity!" if tautologous?
      raise "Identity substitutions can only be carried out on a literal or identity" unless wff.literal? || wff.is_a?(Identity)
      raise "A new wff is required" unless new_wff
      wffs = [[ new_wff ]]
      Decomposition.new(:identity, wffs)
    end
  end


end