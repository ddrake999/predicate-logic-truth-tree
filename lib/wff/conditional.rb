# encoding: UTF-8

module TruthTree


  # a conditional of two wffs
  class Conditional < Wff
    def decompose
      antecedent, consequent = subs
      neg_antecedent = antecedent.negate
      if neg_antecedent == consequent
        wffs = [[consequent]]
      else
        wffs = [ [ neg_antecedent ], [consequent] ]
      end
      Decomposition.new(:arrow, wffs)
    end
  end

end