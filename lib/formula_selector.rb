# encoding: UTF-8

module TruthTree

  class FormulaSelector
    attr_reader :tree
    def initialize(tree)
      @tree = tree
      @tested_this_pass = []
      yield self if block_given?
    end

    # The idea is that a user is shown a diagram of a partial decomposition and prompted for a line and node id
    # If the element seleted is a universal, an existential or an identity, other information must be provided
    def get_for_line_and_node( line, node_id, inargs={} )
      outargs = {}
      element = @tree.elements_for_line(line).detect { |elem| elem.node.id == node_id }
      outargs[:leaf_node] = @tree.pending_leaves.detect { |l| l.id = inargs[:leaf_node_id] } if inargs[:leaf_node_id]
      case element.wff
      when Identity
        outargs[:element] = inargs[:element] 
        outargs[:new_wff] = inargs[:new_wff]
      when Universal
        outargs[:name] = inargs[:name]
      end
      return element, outargs
    end

    def get_next
      @tested_this_pass = []
      if (elem = simple_non_branching) || (elem = branching_non_increasing) 
        return elem, {}
      end
      elem, args = universal_closing
      return elem, args if elem
      elem, args = universal
      return elem, args if elem
      elem, args = identity
      return elem, args if elem 
      elem = simple
      return elem, {} if elem
      return nil
    end

    def simple_non_branching
      @tree.pending_leaves.each do |node|
        element = node.first_simple_pending_nonbranching_element
        return element if element
      end
      return nil
    end

    # Branching elements, including existentials, only need to be checked once for non-increasing, 
    # though they may be included in the elements of several pending leaves.  
    def branching_non_increasing
      @tree.pending_leaves.each do |node|
        (node.simple_pending_branching_elements - @tested_this_pass).each do |elem| 
          return elem if branch_count_increase(elem) <= 0
          @tested_this_pass << elem
        end
      end
      return nil
    end

    # Universals must be checked in the context of each pending node.
    def universal_closing
      @tree.pending_leaves.each do |node|
        node.pending_universals.each do |elem|
          name = node.get_name_for_universal(elem.wff)
          args = {name: name, leaf_node: node }
          return elem, args  if branch_count_increase(elem, args ) < 0
        end
      end
      return nil
    end

    def universal
      @tree.pending_leaves.each do |node|
        if (elem = node.pending_universals.first)
          name = node.get_name_for_universal(elem.wff)
          args = {name: name, leaf_node: node }
          return elem, args
        end
      end
      return nil
    end

    def identity
      @tree.pending_leaves.each do |node|
        idents = node.pending_identities
        idents.each do |id|
          elems = node.eligible_elements_for_identity( id.wff )
          elems.each do |elem|
            new_wff = node.first_wff_not_in_node_elements(elem.name_substitutions(id.wff))
            return id, { element: elem, new_wff: new_wff, leaf_node: node } if new_wff
          end
        end
      end
      nil
    end

    def simple
      element = nil
      @tree.pending_leaves.each do |node|
        break if (element = node.first_simple_pending_element)
      end
      element
    end

    def any
      @tree.pending_leaves.each do |node|
        element = node.first_pending_element
        return element, {} if element
      end
      nil
    end

    def branch_count_increase(element, args={})
      largs = args.dup
      largs[:test] = true
      new_count = @tree.decompose(element, largs) { |t| t.pending_leaves.count }
      return new_count - @tree.pending_leaves.count 
    end

  end


end   
