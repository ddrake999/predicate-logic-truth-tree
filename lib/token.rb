# encoding: UTF-8

module TruthTree
  # A Token represents an item from the vocabulary of predicate logic
  # Its sub-types are: Variable, Name, PredicateLetter, IdentitySymbol, 
  #   Connective, Quantifier and Paren
  # Every token has an associated symbol 
  # An expression is essentially a list of tokens

  class Token
    attr_reader :symbol

    VALID = { 
      predicate_letter: /\A[A-Z]\d*\z/,
      variable:         /\A[u-z]\d*\z/,
      name:             /\A[a-dt]\d*\z/, #t1, t2, ... are reserved for internal use
      paren:            /\A[\)\(}{\]\[]\z/,
      quantifier:       /\A[∀∃]\z/,
      connective:       /\A[~∨&→↔]\z/,
      identity_symbol:  /\A[=≠]\z/
     }

    def initialize(symbol)
      @symbol = symbol
      @string = get_string(symbol)
    end

    def to_s
      @string
    end

    def get_string(symbol)
      case symbol
        when :paren_open    then "("
        when :paren_close   then ")"
        when :eq            then "="
        when :ne            then "≠"
        when :tilde         then "~"
        when :wedge         then "∨"
        when :ampersand     then "&"
        when :arrow         then "→"
        when :double_arrow  then "↔"
        when :exists        then "∃"
        when :forall        then "∀"
        else symbol.to_s
      end
    end

    def ==(other)
      self.class == other.class && self.symbol == other.symbol
    end

    def self.from_string(string)
      case string
      when VALID[:predicate_letter]
        PredicateLetter.new PredicateLetter.symbol_from_string(string)
      when VALID[:variable]
        Variable.new Variable.symbol_from_string(string)
      when VALID[:name]
        Name.new Name.symbol_from_string(string)
      when VALID[:paren]
        Paren.new Paren.symbol_from_string(string)
      when VALID[:quantifier]
        Quantifier.new Quantifier.symbol_from_string(string)
      when VALID[:connective]
        Connective.new Connective.symbol_from_string(string)
      when VALID[:identity_symbol]
        IdentitySymbol.new IdentitySymbol.symbol_from_string(string)
      else
        raise ArgumentError.new "Invalid Token: '#{string}'"
      end
    end

    def self.from_sym(symbol)
      case symbol
      when :tilde, :ampersand, :wedge, :arrow, :double_arrow
        Connective.new(symbol)
      when :paren_open, :paren_close
        Paren.new(symbol)
      when :forall, :exists
        Quantifier.new(symbol)
      when :eq, :ne
        IdentitySymbol.new(symbol)
      else
        case symbol.to_s
        when VALID[:predicate_letter]
          PredicateLetter.new(symbol)
        when VALID[:variable]
          Variable.new(symbol)
        when VALID[:name]
          Name.new(symbol)
        else
          raise ArgumentError.new("Invalid Token: '#{symbol}'")
        end
      end
    end


    # strip whitespace before calling me
    def self.valid?(string)
      !!VALID.detect { |k,v| string =~ v}
    end

    # extract the longest possible valid token from the beginning of the string and return it with the rest
    def self.extract(string)
      s = string.gsub(/\s+/, "")
      sym = s.chars.with_index.map { |c, i| s[0..i] }.reverse.detect{ |sub| valid?(sub) }
      raise ArgumentError.new("'#{string}' doesn't begin with a valid token") unless sym
      [from_string(sym), s[sym.size..-1]]
    end

    def self.tokenize(string, tokens=[])
      s = string.gsub(/\s+/, "")
      return tokens if s.size == 0
      token, rest = extract(s)
      tokenize(rest, tokens << token)
    end

    # convenience methods...
    def self.neg
      from_sym(:tilde)
    end

    def self.p_open
      from_sym(:paren_open)
    end

    def self.p_close
      from_sym(:paren_close)
    end

  end


  # A Variable is a symbol from the following list:
  #    u, v, w, x, y, z, u1, v1, w1, x1, y1, z1, ...
  class Variable < Token
    def self.symbol_from_string(string)
      string.to_sym
    end
  end

  # A Name is a symbol from the following list: 
  # :a, :b, :c, :d, a1, :b1, :c1, :d1, ...
  class Name < Token
    def self.symbol_from_string(string)
      string.to_sym
    end
  end

  # A PredicateLetter is a token which is represented by a capital letter A-Z, A1-Z1, ...
  # It has an integral number of places.  A zero-place predicate is a sentence letter
  class PredicateLetter < Token
    def self.symbol_from_string(string)
      string.to_sym
    end
  end

  # The IdentitySymbol is :eq, its negation is :ne.  Identity is a special predicate.
  class IdentitySymbol < Token
    def self.symbol_from_string(string)
      case string
      when '=' then :eq
      when '≠' then :ne
      else 
        raise ArgumentError.new("#{string} is not a valid IdentitySymbol")
      end
    end
  end

  # There are :arrow, :double_arrow, :tilde, :ampersand, :wedge 
  class Connective < Token
    def self.symbol_from_string(string)
      case string
      when '~' then :tilde
      when '∨' then :wedge
      when '&' then :ampersand
      when '→' then :arrow
      when '↔' then :double_arrow
      else 
        raise ArgumentError.new("#{string} is not a valid Connective")
      end
    end
  end

  # There are two quantifiers 
  #  :forall, the universal quantifier and 
  #  :exists, the existential quantifier.
  class Quantifier < Token
    def self.symbol_from_string(string)
      case string
      when '∀' then :forall
      when '∃' then :exists
      else 
        raise ArgumentError.new("#{string} is not a valid Quantifier")
      end
    end
  end


  # There are :paren_open and paren_close
  class Paren < Token
    def self.symbol_from_string(string)
      case string
      when /[\(\{\[]/ then :paren_open 
      when /[\)\}\]]/ then :paren_close
      else
        raise ArgumentError.new("#{string} is not a valid Paren")
      end
    end
  end

end
