# encoding: UTF-8

module TruthTree

  # a wff is an expression (a sequence of tokens) with a very particular structure
  # it's an abstract class -- every wff instance will be of a specific subtype
  class Wff < Expression

    def initialize(tokens)
      super
      # we're using 'literal' in the sense of "can't be decomposed" so identities are NOT considered literals.
      @is_literal = self.class == AtomicSentence || Wff.negated_identity?(@tokens) ||
        Wff.negated_atomic_sentence?(@tokens)  || Wff.tautologous_identity?(@tokens)

      if @tokens.count > 1 && @tokens[0].symbol == :tilde && ( Wff.identity?(@tokens[1..-1]) )
        @tokens = Wff.toggle_identity(tokens[1..-1])
      end
    end

    # ----------------
    # Class Methods
    # ----------------
    class << self

      def from_tokens(tokens)
        if identity? tokens
          Identity.new(tokens)
        elsif atomic_sentence? tokens
          AtomicSentence.new(tokens)
        elsif negation? tokens
          Negation.new(tokens)
        elsif conjunction? tokens
          Conjunction.new(tokens)
        elsif disjunction? tokens
          Disjunction.new(tokens)
        elsif conditional? tokens
          Conditional.new(tokens)
        elsif biconditional? tokens
          Biconditional.new(tokens)
        elsif universal? tokens
          Universal.new(tokens)
        elsif existential? tokens
          Existential.new(tokens)
        else
          raise "Not a WFF! #{tokens}"
        end
      end

      def from_symbols( symbols )
        from_tokens( get_tokens(symbols) )
      end

      def from_string( string )
        expr = Expression.from_string( string )
        from_tokens( expr.tokens )
      end
  
      def wff?( tokens )
        identity?(tokens)       || atomic_sentence?(tokens) || negation?(tokens)    || 
        conjunction?(tokens)    || disjunction?(tokens)     || conditional?(tokens) || 
        biconditional?(tokens)  || universal?(tokens)       || existential?(tokens)
      end

      def identity?( tokens )
        tokens.size == 3 && tokens[0].is_a?( Name ) && tokens[1].symbol == :eq && tokens[2].is_a?( Name ) 
      end

      def negated_identity?(tokens)
        (tokens.size == 3 && tokens[0].is_a?( Name ) && tokens[1].symbol == :ne && tokens[2].is_a?( Name ) ) ||
        (tokens.size == 4 && tokens[0].symbol == :tilde && identity?(tokens[1..-1]) )
      end

      def tautologous_identity?( tokens )
        identity?(tokens) && (tokens[0] == tokens[2])
      end

      def contradictory_negated_identity?(tokens)
        tokens.size == 3 && tokens[0].is_a?( Name ) && tokens[1].symbol == :ne && 
          tokens[2].symbol == tokens[0].symbol
      end

      def atomic_sentence?( tokens )
        return false unless tokens[0].is_a? PredicateLetter
        return ! tokens[1..-1].find { |t| ! t.is_a?(Name) }
      end

      def negated_atomic_sentence?(tokens)
        tokens.size >= 2 && tokens[0].symbol == :tilde && atomic_sentence?(tokens[1..-1])
      end

      def negation?( tokens )
        return true if Wff.negated_identity?(tokens)
        return tokens.size >= 2 && tokens.first.symbol == :tilde && wff?( tokens[1..-1] )
      end

      def conjunction?( tokens )
        return tokens.size >= 5 && parens?(tokens) && connected_by?(tokens, :ampersand)
      end

      def disjunction?( tokens )
        return tokens.size >= 5 && parens?(tokens) && connected_by?(tokens, :wedge)
      end

      def conditional?( tokens )
        return tokens.size >= 5 && parens?(tokens) && connected_by?(tokens, :arrow)
      end

      def biconditional?( tokens )
        return tokens.size >= 5 && parens?(tokens) && connected_by?(tokens, :double_arrow)
      end

      def universal?( tokens )
        return tokens.size >= 4 && tokens.first.symbol == :forall && var_replacement_is_wff(tokens)
      end

      def existential?( tokens )
        return tokens.size >= 4 && tokens.first.symbol == :exists && var_replacement_is_wff(tokens)
      end

      def last_idx_of_first_wff( tokens )
        wff = tokens.each_index.map { |i| tokens[0..i] }.reverse.detect { |expr| wff?(expr) }
        wff ? wff.size - 1 : nil
      end

      def connected_by?(tokens, symbol)
        return false unless ( idx = last_idx_of_first_wff( tokens[1..-2] ) )
        idx +=1 # to get index within whole tokens array
        tokens.length > idx + 2 && tokens[idx + 1].symbol == symbol && wff?( tokens[idx+2..-2] )
      end

      def var_replacement_is_wff(tokens)
        var = tokens[1] 
        return false unless var.is_a?(Variable) && tokens[2..-1].find { |t| t.symbol == var.symbol }
        # replace all occurences of the variable in the remaining tokens with an arbitrary name
        name = Token.from_sym(:a)
        rest = replacement(tokens[2..-1], var, name)
        return wff?( rest )
      end

      def replacement(tokens, name_or_variable, name)
        tokens.map { |t| t.symbol == name_or_variable.symbol ? name : t }
      end

      def replacement_for_indices(tokens, name, indices)
        tokens.map.with_index { |t, i| indices.include?(i) ? name : t }
      end

      def toggle_identity(tokens)
        [ tokens[0], Token.from_sym( tokens[1].symbol == :eq ? :ne : :eq ), tokens[2] ]
      end
    end

    # ----------------
    # Instance Methods
    # ----------------
    def negate
      if Wff.identity?(tokens) || Wff.negated_identity?(tokens)
        Wff.from_tokens( Wff.toggle_identity(tokens) )
      elsif tokens.count > 1 && tokens[0].symbol == :tilde
        Wff.from_tokens(tokens[1..-1]) 
      else
        Wff.from_tokens([Token.from_sym(:tilde)] + tokens)
      end
    end

    def literal?
      @is_literal
    end

    def subs
      @subs ||= get_subs
    end

    def get_subs
      raise_error "Can't get subexpressions for #{self.class}" unless 
        [Conjunction, Disjunction, Conditional, Biconditional].include?(self.class)
      idx = Wff.last_idx_of_first_wff(@tokens[1..-2]) + 1
      [ Wff.from_tokens(@tokens[1..idx]), Wff.from_tokens(@tokens[idx+2..-2]) ]
    end

    def has_simple_non_branching_decomp?
      raise "A literal has no decomposition" if @is_literal
      return self.is_a?(Conjunction)
    end

  end

end