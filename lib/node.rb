# encoding: UTF-8

module TruthTree

  class Node
    attr_reader :tree, :parent, :last_new_name_idx, :branch_lbl, :closing_element
    attr_accessor :elements, :state, :id, :universal_instantiations, :last_new_name_idx

    def initialize( tree, parent=nil )
      @tree = tree
      @id = tree.next_node_id
      @parent = parent
      @elements = []
      # a node can have three possible states: :pending, :open_complete and :closed
      # only the states of leaf nodes are relevant.
      @state = :pending
      @last_new_name_idx = parent ? parent.last_new_name_idx : 1
      @universal_instantiations = parent ? parent.clone_universal_instantiations : Hash.new {[]}
    end

    def clone_universal_instantiations
      clone = Hash.new {[]}
      @universal_instantiations.each { |k, v| clone[k] = v.dup }
      clone
    end

    def branch_lbl
      @id.to_s + (@parent ? "(" + @parent.id.to_s + ")" : "")
    end

    def to_s
      all_elements.to_s
    end 

    def set_elements(decomposition, index)
      line = @tree.last_line + 1
      wffs = decomposition.wffs[index]
      @elements = wffs.map.with_index { |wff, i| Element.new(self, wff, decomposition, line + i)}
      @closing_element = nil
      @elements.each do |elem| 
        if closed_by?(elem.wff) then @state = :closed; @closing_element = elem end
        @universal_instantiations[elem.wff] = [] if elem.wff.is_a?(Universal)
      end
      @state = :open_complete if open_complete?
      self
    end


    def open_complete?
      @open_complete || ( @state != :closed && !all_elements.detect { |elem| elem.pending?(self) } )
    end

    def closed_by?( wff )
      return false unless wff.literal? || wff.is_a?(Identity)
      return true if Wff.contradictory_negated_identity?(wff.tokens)
      neg = wff.negate
      literals.detect { |l| l.wff == neg } 
    end

    def all_elements
      @all_elements ||=
      if parent
        parent.all_elements + @elements
      else
        @elements
      end
    end

    def first_simple_pending_element
      all_elements.detect { |elem| elem.pending? && ! [Universal, Identity].include?(elem.wff.class) }      
    end

    def first_simple_pending_nonbranching_element
      all_elements.detect do |elem| 
        elem.pending? && ! [Universal, Identity].include?(elem.class) && elem.simple_nonbranching_decomp?
      end   
    end

    def simple_pending_branching_elements
      all_elements.select do |elem| 
        elem.pending? && ! [Universal, Identity].include?(elem.wff.class) && ! elem.simple_nonbranching_decomp?
      end    
    end

    def simple_pending_elements
      all_elements.select { |elem| elem.pending? && ! [Universal, Identity].include?(elem.class) }
    end

    def pending_elements
      all_elements.select { |elem|  elem.pending?(self) }
    end


    def pending_universals
      all_elements.select { |elem|  elem.pending?(self) && elem.wff.is_a?(Universal) }
    end

    def get_name_for_universal(universal)
      return new_name if names.empty?
      uninstantiated_name(universal) 
    end

    def get_all_names_for_universal(universal)
      return [new_name] if names.empty?
      uninstantiated_names(universal) 
    end

    def universal_pending?(universal)
      @universal_instantiations[universal].empty? || !! uninstantiated_name(universal) 
    end

    def uninstantiated_name(universal)
      raise "Not a universal (maybe you tried passing an element?)" unless universal.is_a?(Universal)
      names.detect { |n| ! @universal_instantiations[universal].include?(n) }
    end

    def uninstantiated_names(universal)
      raise "Not a universal (maybe you tried passing an element?)" unless universal.is_a?(Universal)
      names.select { |n| ! @universal_instantiations[universal].include?(n) }
    end

    def pending_identities
      all_elements.select { |elem|  elem.pending?(self) && elem.wff.is_a?(Identity) }
    end

    def literals
      @literals ||= all_elements.select { |elem| elem.wff.literal? }
    end

    def contains_wff?(wff)
      all_elements.detect { |elem| elem.wff == wff }
    end

    def first_wff_not_in_node_elements(wffs)
      wffs.detect { |w| ! contains_wff?(w) }
    end

    def wffs_not_in_node_elements(wffs)
      wffs.select { |w| ! contains_wff?(w) }
    end

    # the unique names referenced in all elements, 
    # needed for universal, existential and identity decompositions
    def names
      return @names if @names
      @names = name_syms.map { |ns| Token.from_sym(ns) }
    end

    def name_syms
      return @name_syms if @name_syms
      name_syms = all_elements.inject([]) { |names, elem| names + elem.wff.unique_name_syms }.uniq
    end

    def eligible_elements_for_identity( ident )
      raise "Not an identity (maybe you tried passing an element?)" unless ident.is_a?(Identity)  
      literals_with_name(ident.left_name) + identities_with_name(ident)
    end

    def literals_with_name( name )
      literals.select { |elem| elem.eligible_for_substitutions? && elem.wff.tokens.include?(name) }
    end

    def identities_with_name( ident )
      all_elements.select do |elem| 
        elem.wff.is_a?(Identity) && (elem.wff != ident) && elem.eligible_for_substitutions? && elem.wff.tokens.include?(ident.left_name) 
      end
    end

    def non_identity_literals
      literals.select { |elem| ! Wff.negated_identity?(elem.wff.tokens) }
    end

    # names t1, t2, ... are reserved for internal use
    def new_name
      name = Token.from_string("t#{@last_new_name_idx}") 
      return name unless names.include?(name)
      @last_new_name_idx += 1
      Token.from_string("t#{@last_new_name_idx}")
    end

    def contains_element?(element)
      all_elements.include?(element)
    end

    def element_for_line(line)
      all_elements.detect { |bw| bw.line == line }
    end




  end

end

