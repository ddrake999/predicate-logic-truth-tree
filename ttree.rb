# encoding: UTF-8
require_relative './lib/token.rb'
require_relative './lib/expression.rb'
require_relative './lib/decomposition.rb'
require_relative './lib/wff.rb'
require_relative './lib/wff/atomic_sentence.rb'
require_relative './lib/wff/biconditional.rb'
require_relative './lib/wff/conditional.rb'
require_relative './lib/wff/conjunction.rb'
require_relative './lib/wff/disjunction.rb'
require_relative './lib/wff/existential.rb'
require_relative './lib/wff/identity.rb'
require_relative './lib/wff/negation.rb'
require_relative './lib/wff/universal.rb'
require_relative './lib/tree.rb'
require_relative './lib/node.rb'
require_relative './lib/element.rb'
require_relative './lib/formula_selector.rb'
require_relative './lib/diagram.rb'
require 'ruby-debug'
# require 'readline'
include TruthTree

def element_for_line_and_node_id(tree, line, node_id=nil)
  elements = tree.incomplete_elements_for_line(line.to_i)
  element = if elements.empty?
    nil
  elsif elements.count == 1
    elements.first 
  else
    elements.detect { |elem| elem.node.id == node_id.to_i }
  end
  element ? (element.belongs_only_to_closed_leaves? ? nil : element) : nil
end

def get_leaf_node(tree, element)
  if (pending_leaves = tree.pending_leaves.select{ |node| node.contains_element?(element) }).count == 1 
    return pending_leaves.first 
  end
  puts "leaf node number for the decomposition"
  input = $stdin.gets
  pending_leaves.detect{ |node| node.id == input.to_i }
end

def get_name_for_decomposition(leaf_node, element)
  names = leaf_node.get_all_names_for_universal(element.wff)
  return nil if names.empty?
  return names.first if names.count == 1
  puts "name for the decomposition: " + names.map.with_index { |n, i| "(#{i+1}) #{n}"}.join(", ")
  input = $stdin.gets
  names[input.to_i - 1]
end

def get_element_for_substitution(leaf_node, identity_elem)
  elements = leaf_node.eligible_elements_for_identity( identity_elem.wff )
  return nil if elements.empty?
  return elements.first if elements.count == 1
  puts "element for name substitution: " + elements.map.with_index { |elem, i| "(#{i+1}) line #{elem.line}. #{elem}" }.join(", ")
  input = $stdin.gets
  elements[input.to_i - 1]
end

def get_new_wff_for_substitution(node, identity_elem, subst_elem)
  wffs = node.wffs_not_in_node_elements(subst_elem.name_substitutions(identity_elem.wff))
  return nil if wffs.empty?
  return wffs.first if wffs.count == 1
  puts "new wff after name substitution: " + wffs.map.with_index { |wff, i| "(#{i+1}) #{wff}"}.join(", ")
  input = $stdin.gets
  wffs[input.to_i - 1]
end

def info(tree)
  pending_leaves = tree.pending_leaves
  pls = pending_leaves.map { |node| node.id }.join(", ")
  pending_leaves.empty? ? "Tree is complete." : "Tree has #{pending_leaves.count} pending leaves: #{pls}"
end

def info_node(tree, node_id)
  node = tree.leaves.detect{ |node| node.id == node_id }
  return "#{node_id} is not the id of a leaf node" unless node
  return "Leaf node #{node_id} is closed" if node.state == :closed
  return "Leaf node #{node_id} is open complete" if node.state == :open_complete
  info = "Pending elements: " 
  info += node.pending_elements.map { |elem| "line #{elem.line}. #{elem}" }.join(", ")
  info += "\nNames: " + node.names.join(", ")
  # info += "\nUniversal instantiations: #{node.universal_instantiations}"
end

help_text = <<EOT
Please specify a file containing statements to process
Example usage: ruby ttree.rb data/samples/S85
EOT

def process_formula(tree, line, node_id)
  element = element_for_line_and_node_id(tree, line, node_id)
  if element
    args = {}
    if [Universal, Identity].include?(element.wff.class)
      unless (args[:leaf_node] = get_leaf_node(tree, element) )
        puts "Leaf node not found"
        return false
      end
    end
    case element.wff
    when Universal
      args[:name] = get_name_for_decomposition(args[:leaf_node], element)
      unless args[:name]
        puts "That universal has already been instantiated in favor of all names in the branch"
        return false
      end
    when Identity
      args[:element] = get_element_for_substitution(args[:leaf_node], element)
      args[:new_wff] = get_new_wff_for_substitution(args[:leaf_node], element, args[:element])
    end
    tree.decompose(element, args)
    tree.display
    puts info(tree)
  else
    puts "Element not found (maybe all formulas on that line are complete or a node wasn't specified)"    
    return false
  end
  return true
end
#-------------
# MAIN PROGRAM
#-------------

abort(help_text) if ARGV.empty?
lines = ARGF.lines.map { |l| l.strip }.select { |l| l.length > 0 && !(l =~ /\A#/) }
tree = Tree.from_lines(lines)
formula_selector = FormulaSelector.new(tree)
tree.display
puts info(tree)
input = nil
until input =~ /\Aq/i
  puts "Please select an option:"
  puts (prompt = "r(un) [to line], f(ormula) <line> [node], l(ast line) [node], i(nfo) <node>, a(gain), q(uit)")
  input = $stdin.gets
  command = input.chars.first
  arguments = input[1..-1].split
  case command
  when /\Ar/i
    if arguments.size > 0
      tree.run(max_lines: arguments.shift.to_i)
    else
      tree.run
    end
    puts info(tree)
  when /\Aa/i
    tree = Tree.from_lines(lines)
    formula_selector = FormulaSelector.new(tree)
    tree.display
    puts info(tree)
  when /\Ai/i
    if arguments.empty?
      puts "Please specify a leaf node id for info"
    else
      puts info_node(tree, arguments.shift.to_i)
    end
  when /\Af/i
    line, node_id = arguments
    next unless process_formula(tree, line, node_id)
  when /\Al/i
    line = tree.last_line
    node_id = arguments.first
    next unless process_formula(tree, line, node_id)
  when /\Aq/i
  else
    puts "Unknown command '#{command}'"
  end
end

