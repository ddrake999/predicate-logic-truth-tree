# encoding: UTF-8

require_relative './spec_helper'

describe "Expression" do
  describe "constructing an instance" do

    describe "from a string" do
      describe "if the string is empty" do
        @expression = Expression.from_string("")
      end
      describe "consisting of valid tokens" do
        it "should have the tokens" do
          @expression = Expression.from_string("A17& x(}~ ~")
          @expression.tokens.size.should eql(7)
        end
      end
      describe "with an invalid token" do
        it "should raise an error" do
          expect { Expression.from_string("z3 & 42 =") }.to raise_error 
        end
      end
    end

    describe "from an array of tokens" do
      it "should have the tokens" do
        @expression = Expression.from_symbols( [:P1, :ampersand, :Q, :tilde] )
        @expression.tokens.size.should eql(4)
        @expression.tokens[0].symbol.should eql(:P1)
      end
    end

  end

  describe ":to_s" do
    it "should return a string representation of the expression" do
      @expression = Expression.from_string("A17& x(}~ ~")
      @expression.to_s.should == 'A17 & x () ~~'
      @expression = Expression.from_symbols(
        [:forall, :x, :paren_open, :P, :a, :x, :double_arrow, :tilde, 
         :paren_open, :Q, :wedge, :P, :paren_close, :paren_close])
      @expression.to_s.should == '∀x (Pax ↔ ~(Q ∨ P))'
      @expression = Expression.from_string("((~∀x∀yPxy→Qy)&Pa) ")
      @expression.to_s.should == '((~∀x∀yPxy → Qy) & Pa)'
    end
  end

  describe ":==" do 
    describe "when the symbol lists are the same" do 
      it "should be true" do
        @exp1 = Expression.from_symbols( [:P, :a])
        @exp2 = Expression.from_symbols( [:P, :a])
        @exp1.should == @exp2
      end
    end
    describe "when the symbol lists are different" do 
      it "should be true" do
        @exp1 = Expression.from_symbols( [:paren_open, :P, :wedge, :Q, :paren_close])
        @exp2 = Expression.from_symbols( [:paren_open, :Q, :wedge, :P, :paren_close])
        @exp1.should_not == @exp2
      end
    end
  end
  
  describe ":negate" do 
    it "should return a negation of the Wff" do
      Wff.from_symbols( [:P] ).negate.should == Wff.from_symbols( [:tilde, :P] )
    end
  end

  describe ":unique_name_syms" do
    it "should return a unique list of names referenced by the expression" do
      @exp = Expression.from_symbols( [:paren_open, :P, :a, :b, :double_arrow, :Q, :b, :paren_close])
      @exp.unique_name_syms.should eql([:a, :b])
      @exp = Wff.from_symbols( [:forall, :x, :P, :x])
      @exp.unique_name_syms.should eql([])
    end
  end

  describe ":name_syms" do
    it "should return a complete ordered list of names referenced by the expression" do
      @exp = Expression.from_symbols( [:paren_open, :P, :a, :b, :double_arrow, :Q, :b, :paren_close])
      @exp.name_syms.should eql([:a, :b, :b])
      @exp = Wff.from_symbols( [:forall, :x, :P, :x])
      @exp.name_syms.should eql([])
    end
  end

  describe ":symbol_indices" do
    describe "when passed a symbol" do
      it "should return the indices of the symbol in the expression" do
        @exp = Expression.from_symbols( [:paren_open, :P, :a, :ampersand, :paren_open, :P, :b, 
                                  :wedge, :G, :a, :paren_close, :paren_close])
        @exp.symbol_indices(:a).should == [2,9]
      end
    end
    describe "when passed a token" do
      it "should return the indices of the token's symbol in the expression" do
        @exp = Expression.from_symbols( [:paren_open, :P, :a, :ampersand, :paren_open, :P, :b, 
                                  :wedge, :G, :a, :paren_close, :paren_close])
        @exp.symbol_indices(Token.from_sym(:a)).should == [2,9]
      end
    end
  end

end

