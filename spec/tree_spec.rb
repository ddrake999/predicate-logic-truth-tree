# encoding: UTF-8

require_relative './spec_helper'

describe "Tree" do

  describe ":initialize" do
    it "should have an empty collection of leaves" do
      @tree = Tree.new
      @tree.leaves.should == []
    end

  end

  describe ":from_symbols" do
    describe "when the symbols represent an open branch" do 
      its "leaves should have one node" do
        @tree = Tree.from_symbols( [
          [:P],
          [:paren_open, :P, :arrow, :Q, :paren_close],
        ] )
        @tree.leaves.count.should == 1
        @leaf = @tree.leaves.first
        @leaf.elements.first.wff.should == Wff.from_symbols([:P])
        @leaf.state.should == :pending
      end
    end
    describe "when the symbols represent a closed branch" do 
      its "leaves should have one node" do
        @tree = Tree.from_symbols( [
          [:P],
          [:tilde, :P],
        ] )
        @tree.leaves.count.should == 1
        @leaf = @tree.leaves.first
        @leaf.elements.first.wff.should == Wff.from_symbols([:P])
        @leaf.state.should == :closed
      end
    end
    describe "when the symbols represent an open complete branch" do 
      its "leaves should have one node" do
        @tree = Tree.from_symbols( [
          [:P],
          [:Q],
        ] )
        @tree.leaves.count.should == 1
        @leaf = @tree.leaves.first
        @leaf.elements.first.wff.should == Wff.from_symbols([:P])
        @leaf.state.should == :open_complete
      end
    end
  end

  describe ":open_complete_leaves" do 
    it "should return the open leaves" do
      @tree = Tree.from_lines ['Q', '~P']
      @tree.open_complete_leaves.count.should eql(1)
    end
  end


  describe ":decompose" do
    describe "with a simple tree" do
      it "should replace the original leaf with two new leaves" do
        @tree = Tree.from_lines ['(P→Q)','~Q']
        @tree.leaves.count.should == 1
        @leaf = @tree.leaves.first
        @leaf.state.should == :pending
        @element = @leaf.elements.first
        @tree.decompose(@element)
        @tree.leaves.count.should == 2
      end
    end
    describe "when not testing" do
      describe "when the wff is not pending" do
        it "should raise an error" do 
          @tree = Tree.from_lines ['(Q ∨ P)', '~P']
          @tree.last_line.should eql(2)
          @leaf = @tree.leaves[0]
          @tree.decompose( @leaf.elements[0])
          @tree.leaves.count.should eql(2)
          @tree.closed_leaves.count.should eql(1)
          @tree.open_complete_leaves.count.should eql(1)
          @tree.pending_leaves.count.should eql(0)
          @tree.last_line.should eql(3)
          expect do 
            @tree.decompose( @leaf.elements[0]) 
          end.to raise_error
        end
      end        
      describe "when the wff is present in only one branch" do
        describe "when the wff has a branching decomposition" do
          it "should add a new branch" do 
            @wffs = [ Wff.from_symbols([:paren_open, :Q, :wedge, :P, :paren_close]), 
                      Wff.from_symbols([:tilde, :P]) ]
            @tree = Tree.from_wffs(@wffs)
            @tree.last_line.should eql(2)
            @leaf = @tree.leaves[0]
            @wff = @leaf.elements[0]
            @tree.decompose( @wff )
            @leaf1 = @tree.leaves[0]
            @leaf2 = @tree.leaves[1]
            @tree.last_line.should eql(3)
            @tree.leaves.count.should eql(2)
            @tree.leaves[1].branch_lbl.should eql('3(1)')
            @tree.open_complete_leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(0)
            @tree.open_complete_leaves[0].all_elements.count.should eql(3)
            @tree.open_complete_leaves[0].all_elements[2].wff.should == Wff.from_string('Q')
            @wff.pending?.should eql(false)
            @leaf1.element_for_line(3).to_s.should == 'Q'
            @leaf2.element_for_line(3).to_s.should == 'P'
            @tree.elements_for_line(3).count.should eql(2)
          end
        end
        describe "when the wff has a non-branching decomposition" do
          describe "with an ampersand" do
            it "should not add a new branch" do 
              @wffs = [ Wff.from_symbols([:paren_open, :Q, :ampersand, :P, :paren_close]), 
                        Wff.from_symbols([:tilde, :P]) ]
              @tree = Tree.from_wffs(@wffs)
              @leaf = @tree.leaves[0]
              @wff = @leaf.elements[0]
              @tree.decompose(  @wff )
              @tree.last_line.should eql(4)
              @tree.leaves.count.should eql(1)
              @tree.open_complete_leaves.count.should eql(0)
              @tree.pending_leaves.count.should eql(0)
              @leaf = @tree.leaves[0]
              @leaf.all_elements.count.should eql(4)
              @leaf.all_elements.last.wff.class.should eql(AtomicSentence)
              @wff.pending?.should eql(false)
            end
          end
          describe "with a negated arrow" do
            it "should not add a new branch" do 
              @wffs = [ Wff.from_symbols([:tilde, :paren_open, :Q, :arrow, :P, :paren_close]), 
                        Wff.from_symbols([:tilde, :P]) ]
              @tree = Tree.from_wffs(@wffs)
              @leaf = @tree.leaves[0]
              @wff = @leaf.elements[0]
              @tree.decompose(  @wff )
              @tree.last_line.should eql(4)
              @tree.leaves.count.should eql(1)
              @tree.open_complete_leaves.count.should eql(1)
              @tree.pending_leaves.count.should eql(0)
              @leaf = @tree.leaves[0]
              @leaf.all_elements.count.should eql(4)
              @leaf.all_elements.last.wff.class.should eql(Negation)
              @wff.pending?.should eql(false)
            end
          end
        end
      end
      describe "when the wff is present in two open leaves" do
        before(:each) do
          @wffs = [ 
            Wff.from_symbols([:paren_open, :Q, :wedge, :R, :paren_close]), 
            Wff.from_symbols([:paren_open, :P, :wedge, :R, :paren_close]), 
            Wff.from_symbols([:paren_open, :S, :ampersand, :R, :paren_close]), 
            Wff.from_symbols([:tilde, :S]) ]
          @tree = Tree.from_wffs(@wffs)
          @leaf = @tree.leaves[0]
          @tree.decompose( @leaf.elements[0] )
          @leaf = @tree.leaves[0]
        end
        describe "when the wff has a branching decomposition" do
          it "should add a new branch to each of the existing open leaves" do 
            @tree.leaves.count.should eql(2)
            @tree.pending_leaves.count.should eql(2)
            @tree.last_line.should eql(5)
            @wff1 = @leaf.all_elements[1]
            @tree.decompose( @wff1 )
            @tree.last_line.should eql(6)
            @tree.leaves.count.should eql(4)
            @tree.open_complete_leaves.count.should eql(0)
            @tree.pending_leaves.count.should eql(4)
            @tree.leaves[1].branch_lbl.should eql('5(2)')
            @tree.leaves[3].branch_lbl.should eql('7(3)')
            @tree.pending_leaves[0].all_elements.count.should eql(6)
            @leaf = @tree.leaves[0]
            @leaf.all_elements.last.wff.should == Wff.from_string('P')
            @wff1.pending?.should eql(false)
          end
        end
        describe "when the wff has a non-branching decomposition" do
          it "should add its formulas to both of the existing open leaves" do
            @tree.leaves.count.should eql(2)
            @tree.pending_leaves.count.should eql(2)
            @wff2 = @leaf.all_elements[2]
            @tree.decompose( @wff2 )
            @tree.last_line.should eql(7)
            @tree.leaves.count.should eql(2)
            @tree.pending_leaves.count.should eql(0)
            @leaf = @tree.leaves[0]
            @leaf.all_elements.count.should eql(7)
            @leaf.all_elements.last.wff.class.should eql(AtomicSentence)
            @wff2.pending?.should eql(false)
          end
        end
      end
      describe "when the wff is a unversal" do
        describe "which has not been fully intantiated for the branch" do
          before(:each) do
            @wffs = [ 
              Wff.from_symbols([:forall, :x, :P, :x]), 
              Wff.from_symbols([:paren_open, :P, :a, :arrow, :R, :paren_close]), 
              Wff.from_symbols([:tilde, :paren_open, :P, :b, :ampersand, :R, :paren_close]),
               ]
            @tree = Tree.from_wffs(@wffs)
            @leaf = @tree.leaves[0]
          end
          it "should be pending until it has been fully instantiated" do
            @uni = @leaf.elements[0]
            @tree.decompose( @uni, name: Name.new(:a), leaf_node: @leaf )
            @tree.leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(1)
            @uni.pending?(@leaf).should eql(true)
            @leaf = @tree.leaves[0]
            @leaf.all_elements.count.should eql( 4 )
            @leaf.all_elements.last.wff.should == Wff.from_symbols([:P, :a])
            @tree.decompose( @uni, name: Name.new(:b), leaf_node: @leaf )
            @tree.leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(1)
            @leaf = @tree.leaves[0]
            @leaf.all_elements.count.should eql( 5 )
            @leaf.all_elements.last.wff.should == Wff.from_symbols([:P, :b])
            @uni.pending?(@leaf).should eql(false)
          end
        end
      end
      describe "when the wff is an identity" do
        describe "whose substitution has not been applied to all literals in the branch" do
          before(:each) do
            @wffs = [ 
              Wff.from_symbols([:a, :eq, :b]), 
              Wff.from_symbols([:P, :a]), 
              Wff.from_symbols([:tilde, :Q, :a]),
              Wff.from_symbols([:tilde, :R, :b]),
               ]
            @tree = Tree.from_wffs(@wffs)
            @leaf = @tree.leaves[0]
          end
          it "should be pending until its substitution has been fully applied" do
            @ident = @leaf.elements[0]
            @bw1 = @leaf.elements[1]
            @bw2 = @leaf.elements[2]
            @ident.pending?.should eql(true)
            
            @tree.decompose( @ident, element: @bw1, new_wff: Wff.from_symbols([:P, :b]), leaf_node: @leaf )
            @tree.leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(1)
            @leaf = @tree.leaves[0]
            @leaf.all_elements.count.should eql( 5 )
            @leaf.all_elements.last.wff.should == Wff.from_symbols([:P, :b])
            @ident.pending?(@leaf).should eql(true)
            @tree.decompose( @ident, element: @bw2, new_wff: Wff.from_symbols([:tilde, :Q, :b]), leaf_node: @leaf )
            @tree.leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(0)
            @leaf = @tree.leaves[0]
            @leaf.all_elements.count.should eql( 6 )
            @leaf.all_elements.last.wff.should == Wff.from_symbols([:tilde, :Q, :b])
            @ident.pending?(@leaf).should eql(false)
          end
        end
      end
    end
    # todo: add more tests for Universal and Identity scenarios to ensure that the branch wff is restored 
    # to the correct state
    describe "for a test decomposition" do
      describe "with some branching formulas" do
        before(:each) do
          @wffs = [ 
            Wff.from_symbols([:paren_open, :Q, :wedge, :R, :paren_close]), 
            Wff.from_symbols([:paren_open, :S, :wedge, :T, :paren_close]), 
            Wff.from_symbols([:tilde, :T]) ]
          @tree = Tree.from_wffs(@wffs)
          @leaf = @tree.leaves[0]
          @tree.decompose( @leaf.elements[0] )
          @leaf = @tree.leaves[0]
        end
        it "should maintain the state of the main tree under normal circumstances" do
          @tree.leaves.count.should eql(2)
          @tree.pending_leaves.count.should eql(2)
          @tree.last_line.should eql(4)
          @wff1 = @leaf.all_elements[1]
          @tree.decompose( @wff1, test: true ) do |tree|
            @tree.leaves.count.should eql(2)
            @tree.new_leaves.count.should eql(4)
            tree.pending_leaves.count.should eql(0)
            @wff1.pending?.should eql(false)
          end
          @tree.leaves.count.should eql(2)
          @tree.pending_leaves.count.should eql(2)
          @tree.last_line.should eql(4)
          @wff1.pending?.should eql(true)
        end
        it "should maintain the state of the main tree even if a bomb is dropped!" do
          @wff1 = @leaf.all_elements[1]
          @tree.decompose( @wff1, test: true ) do |tree|
            raise_error "a bomb!"
          end
          @tree.leaves.count.should eql(2)
          @tree.pending_leaves.count.should eql(2)
          @tree.last_line.should eql(4)
          @wff1.pending?.should eql(true)
        end

      end


      describe "with an identity" do
        describe "without specifying indices" do
          before(:each) do
            @wffs = [ 
              Wff.from_symbols([:P, :a]), 
              Wff.from_symbols([:a, :eq, :b]), 
              Wff.from_symbols([:tilde, :P, :b]) ]
            @tree = Tree.from_wffs(@wffs)
            @leaf = @tree.leaves[0]
          end
          it "should maintain the state of the main tree" do
            @tree.leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(1)
            @tree.last_line.should eql(3)
            @wff1 = @leaf.elements[1]
            @wff0 = @leaf.elements[0]
            @tree.decompose( @wff1, element: @wff0, new_wff: Wff.from_symbols([:P, :b]), leaf_node: @leaf, test: true ) do |tree|
              tree.pending_leaves.count.should eql(0)
              tree.new_leaves[0].all_elements[1].pending?(tree.new_leaves[0]).should eql(false)
            end
            @tree.leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(1)
            @tree.last_line.should eql(3)
            @wff1.pending?.should eql(true)
          end
        end

        describe "at specific indices" do
          before(:each) do
            @wffs = [ 
              Wff.from_symbols([:P, :a, :a]), 
              Wff.from_symbols([:a, :eq, :b]), 
              Wff.from_symbols([:tilde, :P, :a, :b]) ]
            @tree = Tree.from_wffs(@wffs)
            @leaf = @tree.leaves[0]
          end
          it "should maintain the state of the main tree" do
            @tree.leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(1)
            @tree.last_line.should eql(3)
            @wff1 = @leaf.elements[1]
            @wff0 = @leaf.elements[0]
            @tree.decompose( @wff1, element: @wff0, new_wff: Wff.from_symbols([:P, :a, :b]), leaf_node: @leaf, test: true) do |tree|
              tree.leaves.count.should eql(1)
              tree.pending_leaves.count.should eql(0)
              @wff1.pending?.should eql(true) # since it hasn't been fully decomposed!
            end
            @tree.leaves.count.should eql(1)
            @tree.pending_leaves.count.should eql(1)
            @tree.last_line.should eql(3)
            @wff1.pending?.should eql(true)
          end
        end

      end


    end

  end

end
