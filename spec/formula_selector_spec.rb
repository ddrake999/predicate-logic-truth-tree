# encoding: UTF-8

require_relative './spec_helper'

describe "FormulaSelector" do
  describe ":initialize" do
    before(:each) do
      @tree = Tree.from_lines( %w{ (P→Qa) ~(P∨R) ∀x(Qx↔~R) })
      @selector = FormulaSelector.new(@tree)
    end
    it "should reference the tree" do
      @selector.tree.class.should eql(Tree)
    end
  end

  describe ":get_for_line_and_node" do
    before(:each) do
      @tree = Tree.from_lines [
        '(P→Qa)',
        '~(P∨R)',
        'Qa',
        '∀x(Qx↔~R)',
        'a=b',
        '∃x(P&Qx)'
      ]
      @selector = FormulaSelector.new(@tree)
    end
    describe "when anything but an identity, an existential or a universal is specified" do
      it "should return an element and an empty args hash" do
        @elem, @args = @selector.get_for_line_and_node(1,1)
        @elem.should eql(@tree.leaves[0].elements[0])
        @args.should == {}
        @tree.decompose(@elem, @args)
        @tree.leaves.count.should eql(2)
      end
    end
    describe "when an identity is specified, along with a leaf node id, an element and a new wff" do
      it "should return an element and an args hash" do
        @subst_elem = @tree.leaves.first.elements[2]
        @new_wff = Wff.from_string('Qb')
        @elem, @args = @selector.get_for_line_and_node(5,1, leaf_node_id: 1, element: @subst_elem, new_wff: @new_wff)
        @elem.should eql(@tree.leaves[0].elements[4])
        @args.should == { leaf_node: @tree.leaves.first, element: @subst_elem, new_wff: @new_wff }
        @tree.decompose(@elem, @args)
        @tree.last_line.should eql(7)
      end
    end
    describe "when a universal is specified, along with a leaf node id and a name" do
      it "should return an element and an args hash" do
        @elem, @args = @selector.get_for_line_and_node(4,1, leaf_node_id: 1, name: Token.from_sym(:a) )
        @elem.should eql(@tree.leaves[0].elements[3])
        @args.should == { leaf_node: @tree.leaves.first, name: Token.from_sym(:a) }
        @tree.decompose(@elem, @args)
        @tree.last_line.should eql(7)
      end
    end
    describe "when an existential is specified, along with a leaf node id and a name" do
      it "should return an element and an args hash" do
        @elem, @args = @selector.get_for_line_and_node(6,1, leaf_node_id: 1 )
        @elem.should eql(@tree.leaves[0].elements[5])
        @args.should == { leaf_node: @tree.leaves.first }
        @tree.decompose(@elem, @args)
        @tree.last_line.should eql(7)
        @tree.leaves.count.should eql(3)
      end
    end
  end

  describe ":get_next" do
    describe "when the tree has pending formulas" do
      before(:each) do
        @tree = Tree.from_lines( %w{ (P→Qa) ~(P∨R) ∀x(Qx↔~R) })
        @selector = FormulaSelector.new(@tree)
      end
      it "should return a branch wff" do
        @bw, *args = @selector.get_next
        @bw.symbols.should == [:tilde, :paren_open, :P, :wedge, :R, :paren_close]
      end
    end
    describe "when the tree doesn't have any pending formulas" do
      before(:each) do
        @tree = Tree.from_lines( %w{ P ~Q a≠b })
        @selector = FormulaSelector.new(@tree)
      end
      it "should return nil" do
        @bw, *args = @selector.get_next
        @bw.should be_nil
      end
    end

  end


  describe ":simple_non_branching" do
    describe "when the tree contains a simple non-branching wff" do
      before(:each) do
        @tree = Tree.from_lines( %w{ (P→Qa) ~(P∨R) ∀x(Qx↔~R) })
        @selector = FormulaSelector.new(@tree)
      end
      it "should return the branch_wff" do
        @branch_wff = @selector.simple_non_branching
        @branch_wff.symbols.should == [:tilde, :paren_open, :P, :wedge, :R, :paren_close]
      end
    end    
    describe "when the tree doesn't contain any simple non-branching wffs" do
      before(:each) do
        @tree = Tree.from_lines( %w{ (P→Qa) ~(P∨R) ∀x(Qx↔~R) })
        @tree.decompose(@tree.leaves[0].elements[1])
        @selector = FormulaSelector.new(@tree)
      end
      it "should return nil" do
        @tree.leaves.count.should eql(1)
        @tree.last_line.should eql(5)
        @element = @selector.simple_non_branching
        @element.should be_nil
      end
    end 
  end

  describe ":branching_non_increasing" do
    describe "when the tree contains a branching wff whose decomposition closes as least as many branches as it creates" do
      describe "for a conditional" do
        before(:each) do
          @tree = Tree.from_lines( %w{ (P→Qa) ~(S&R) ∀x(Qx↔~R) P })
          @tree.decompose(@tree.leaves[0].elements[1])
          @selector = FormulaSelector.new(@tree)
        end
        it "should return the branch wff" do
          @element = @selector.branching_non_increasing   
          @element.symbols.should == [:paren_open, :P, :arrow, :Q, :a, :paren_close]
        end 
      end
      describe "for an existential" do
        before(:each) do
          @tree = Tree.from_lines( %w{ ~Qa ~(S∨R) ∃xQx P })
          @selector = FormulaSelector.new(@tree)
        end
        it "should return the branch wff" do
          @element = @selector.branching_non_increasing   
          @element.symbols.should == [:exists, :x, :Q, :x]
        end 
      end
    end
    describe "when the tree's branching wffs decompositions all create more branches than they close" do
      before(:each) do
        @tree = Tree.from_lines( %w{ (P→Qa) (S&R) ∃x(Qx↔~R) ~P })
        @selector = FormulaSelector.new(@tree)
      end
      it "should return nil" do
        @element = @selector.branching_non_increasing   
        @element.should be_nil
      end 
    end
  end

  describe ":universal" do
    describe "when the tree contains a pending universal" do
      describe "when the branch with the universal has a name" do
        before(:each) do
          @tree = Tree.from_lines( %w{ (P→Qa) ~(S&R) ∀x(Qx↔~Rxx) ~P })
          @selector = FormulaSelector.new(@tree)
        end
        it "should return the branch wff along with the name to replace" do
          @branch_wff, @args = @selector.universal
          @branch_wff.should eql(@tree.leaves[0].elements[2])  
          @args[:name].symbol.should eql(:a)
          @tree.decompose(@branch_wff, @args)
          @tree.last_line.should eql(5)
          @elem1 = @tree.leaves[0].all_elements[4]
          @elem1.symbols.should == [:paren_open, :Q, :a, :double_arrow, :tilde, :R, :a, :a, :paren_close]
        end 
      end
      describe "when the branch with the universal doesn't have any names" do
        before(:each) do
          @tree = Tree.from_lines( %w{ ~(S&R) ∀x(Qx↔~Rxx) P })
          @selector = FormulaSelector.new(@tree)
        end
        it "should return the branch wff along with an auto-generated name to replace" do
          @branch_wff, @args = @selector.universal
          @branch_wff.should eql(@tree.leaves[0].elements[1])  
          @args[:name].symbol.should eql(:t1)
          @tree.decompose(@branch_wff, @args)
          @tree.last_line.should eql(4)
          @elem1 = @tree.leaves[0].all_elements[3]
          @elem1.symbols.should == [:paren_open, :Q, :t1, :double_arrow, :tilde, :R, :t1, :t1, :paren_close]
        end 
      end
    end
  end

  describe ":identity" do
    describe "when the tree contains a pending identity" do
      # TODO: add nested spec to test a branch with multiple identities, etc...
      before(:each) do
        @tree = Tree.from_lines( %w{ Qb ~(Qa&R) Raa a=b })
        @selector = FormulaSelector.new(@tree)
      end
      it "should return the branch wff along with the substitution branch wff and indices" do
        @elem, @args = @selector.identity
        @elem.should eql(@tree.leaves[0].all_elements[3])  
        @args[:element].should eql(@tree.leaves[0].all_elements[2])
        @args[:new_wff].should == Wff.from_symbols([:R, :a, :b])
        @tree.decompose(@elem, @args)
        @tree.last_line.should eql(5)
        @elem1 = @tree.leaves[0].all_elements[4]
        @elem1.symbols.should == [:R, :a, :b]

        @elem, @args = @selector.identity
        @elem.should eql(@tree.leaves[0].all_elements[3])  
        @args[:element].should eql(@tree.leaves[0].all_elements[2])
        @args[:new_wff].should == Wff.from_symbols([:R, :b, :a])
        @tree.decompose(@elem, @args)
        @tree.last_line.should eql(6)
        @bw2 = @tree.leaves[0].all_elements[5]
        @bw2.symbols.should == [:R, :b, :a]
      end 
    end
  end

end

