# encoding: UTF-8

require_relative './spec_helper'

describe "Element" do

  describe ":initialize" do
    before(:each) do
      @tree = Tree.new
      @node = Node.new(@tree)
      @wff = Wff.from_symbols [:P]
      @decomposition = Decomposition.new(:set_member, [[:P]])
      @element = Element.new(@node, @wff, @decomposition, 1)
    end

    it "should have a wff" do
      @element.wff.should eql(@wff)
    end

    it "should belong to a node" do
      @element.node.should eql(@node)
    end

    it "should have a [parent decomposition" do
      @element.parent_decomposition.should eql(@decomposition)
    end

  end


end