# encoding: UTF-8

require_relative './spec_helper'

describe "Wff" do


  # ------------------------------------
  # Checking tokens for sub type match
  # ------------------------------------
  describe ":identity?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.identity?( Wff.get_tokens([:a, :eq, :b]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.identity?( Wff.get_tokens([:P3, :x])  ).should be_false
        Wff.identity?( Wff.get_tokens([:a, :ne, :b])  ).should be_false
      end
    end
  end

  describe ":atomic_sentence?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.atomic_sentence?( Wff.get_tokens([:Q]) ).should be_true
        Wff.atomic_sentence?( Wff.get_tokens([:P3, :a]) ).should be_true
        Wff.atomic_sentence?( Wff.get_tokens([:R, :a, :b]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.atomic_sentence?( Wff.get_tokens([:P3, :x])  ).should be_false
        Wff.atomic_sentence?( Wff.get_tokens([:a, :ne, :b])  ).should be_false
      end
    end
  end

  describe ":negation?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.negation?( Wff.get_tokens([:tilde, :P3, :a]) ).should be_true
        Wff.negation?( Wff.get_tokens([:tilde, :tilde, :Q123, :a]) ).should be_true
        Wff.negation?( Wff.get_tokens([:tilde, :forall, :z, :P3, :z]) ).should be_true
        Wff.negation?( Wff.get_tokens([:tilde, :paren_open, :P, :arrow, :P3, :paren_close]) ).should be_true
        Wff.negation?( Wff.get_tokens([:a, :ne, :b]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.negation?( Wff.get_tokens([:tilde, :P, :wedge, :Q])  ).should be_false
        Wff.negation?( Wff.get_tokens([:P3, :a]) ).should be_false
      end
    end
  end

  describe ":conjunction?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.conjunction?( Wff.get_tokens([:paren_open, :P3, :a, :ampersand, :Q, :paren_close]) ).should be_true
        Wff.conjunction?( Wff.get_tokens([:paren_open, :a, :eq, :b, :ampersand, :Q, :paren_close]) ).should be_true
        Wff.conjunction?( Wff.get_tokens([:paren_open, :tilde, :exists, :x, :P, :x, :ampersand, :Q, :paren_close]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.conjunction?( Wff.get_tokens([:tilde, :P, :wedge, :Q])  ).should be_false
        Wff.conjunction?( Wff.get_tokens([:P3, :a]) ).should be_false
        Wff.conjunction?( Wff.get_tokens([:P, :ampersand, :Q])  ).should be_false
        Wff.conjunction?( Wff.get_tokens([:paren_open, :P, :ampersand, :Q, :ampersand, :R, :paren_close]) ).should be_false
      end
    end
  end

  describe ":disjunction?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.disjunction?( Wff.get_tokens([:paren_open, :P3, :a, :wedge, :Q, :paren_close]) ).should be_true
        Wff.disjunction?( Wff.get_tokens([:paren_open, :P3, :a, :wedge, :a1, :eq, :b3, :paren_close]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.disjunction?( Wff.get_tokens([:tilde, :P, :wedge, :Q])  ).should be_false
        Wff.disjunction?( Wff.get_tokens([:P3, :a]) ).should be_false
        Wff.disjunction?( Wff.get_tokens([:P, :wedge, :Q])  ).should be_false
      end
    end
  end

  describe ":conditional?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.conditional?( Wff.get_tokens([:paren_open, :P3, :a, :arrow, :Q, :paren_close]) ).should be_true
        Wff.conditional?( Wff.get_tokens([:paren_open, :a1, :eq, :b3, :arrow, :P3, :a, :paren_close]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.conditional?( Wff.get_tokens([:tilde, :P, :arrow, :Q])  ).should be_false
        Wff.conditional?( Wff.get_tokens([:P3, :a]) ).should be_false
        Wff.conditional?( Wff.get_tokens([:P, :arrow, :Q])  ).should be_false
      end
    end
  end

  describe ":biconditional?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.biconditional?( Wff.get_tokens([:paren_open, :P3, :a, :double_arrow, :Q, :paren_close]) ).should be_true
        Wff.biconditional?( Wff.get_tokens([:paren_open, :a1, :eq, :b3, :double_arrow, :P3, :a, :paren_close]) ).should be_true
        Wff.biconditional?( Wff.get_tokens([:paren_open, :paren_open, :P, :ampersand, :Q, :paren_close, :double_arrow, :P3, :a, :paren_close]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.biconditional?( Wff.get_tokens([:tilde, :P, :double_arrow, :Q])  ).should be_false
        Wff.biconditional?( Wff.get_tokens([:P3, :a]) ).should be_false
        Wff.biconditional?( Wff.get_tokens([:P, :double_arrow, :Q])  ).should be_false
      end
    end
  end

  describe ":universal?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.universal?( Wff.get_tokens([:forall, :x, :P, :x]) ).should be_true
        Wff.universal?( Wff.get_tokens([:forall, :x, :Q, :a, :x]) ).should be_true
        Wff.universal?( Wff.get_tokens([:forall, :x, :x, :eq, :a]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.universal?( Wff.get_tokens([:forall, :x, :Q])  ).should be_false
        Wff.universal?( Wff.get_tokens([:forall, :x, :y, :eq, :a]) ).should be_false
        Wff.universal?( Wff.get_tokens([:forall, :x, :R1, :y])  ).should be_false
      end
    end
  end

  describe ":existential?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.existential?( Wff.get_tokens([:exists, :x, :P, :x]) ).should be_true
        Wff.existential?( Wff.get_tokens([:exists, :x, :Q, :a, :x]) ).should be_true
        Wff.existential?( Wff.get_tokens([:exists, :x, :x, :eq, :a]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.existential?( Wff.get_tokens([:exists, :x, :Q])  ).should be_false
        Wff.existential?( Wff.get_tokens([:exists, :x, :y, :eq, :a]) ).should be_false
        Wff.existential?( Wff.get_tokens([:exists, :x, :R1, :y])  ).should be_false
      end
    end
  end

  describe ":negated_identity?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.negated_identity?( Wff.get_tokens([:a, :ne, :b]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.negated_identity?( Wff.get_tokens([:P3, :x])  ).should be_false
        Wff.negated_identity?( Wff.get_tokens([:a, :eq, :b])  ).should be_false
      end
    end
  end

  describe ":tautologous_identity?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.tautologous_identity?( Wff.get_tokens([:a, :eq, :a]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.tautologous_identity?( Wff.get_tokens([:a, :ne, :a])  ).should be_false
        Wff.tautologous_identity?( Wff.get_tokens([:a, :eq, :b])  ).should be_false
      end
    end
  end

  describe ":contradictory_negated_identity?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.contradictory_negated_identity?( Wff.get_tokens([:a, :ne, :a]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.contradictory_negated_identity?( Wff.get_tokens([:a, :eq, :a])  ).should be_false
        Wff.contradictory_negated_identity?( Wff.get_tokens([:a, :eq, :b])  ).should be_false
      end
    end
  end

  describe ":negated_atomic_sentence?" do
    describe "with a correct sequence of tokens" do
      it "should return true" do
        Wff.negated_atomic_sentence?( Wff.get_tokens([:tilde, :P, :a]) ).should be_true
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.negated_atomic_sentence?( Wff.get_tokens([:a, :ne, :a])  ).should be_false
        Wff.negated_atomic_sentence?( Wff.get_tokens([:a, :eq, :b])  ).should be_false
      end
    end
  end


  # ------------------------------------
  # Construction from Symbols and Strings
  # ------------------------------------
  describe ":from_symbols" do
    describe "with an invalid sequence of tokens" do
      it "should raise an error" do
        expect { @wff = Wff.from_symbols( [:P, :wedge, :Q]) }.to raise_error
      end
    end

    describe "with a valid sequence of symbols" do
      describe "for an atomic sentence" do
        it "should return an atomic sentence" do
          @wff = Wff.from_symbols( [:P] )
          @wff.class.should eql(AtomicSentence)
          @wff.symbols.should eql([:P])

          @wff = Wff.from_symbols( [:a, :eq, :b] )
          @wff.class.should eql(Identity)
          @wff.symbols.should eql([:a, :eq, :b])

          @wff = Wff.from_symbols( [:P, :a, :b12, :b] )
          @wff.class.should eql(AtomicSentence)
          @wff.symbols.should eql([:P, :a, :b12, :b])
        end
      end
      describe "for a conjunction" do
        it "should return a conjunction" do
          @wff = Wff.from_symbols( [:paren_open, :P, :ampersand, :Q, :paren_close])
          @wff.class.should eql(Conjunction)
          @wff = Wff.from_symbols( [:paren_open, :P, :ampersand, :tilde, :Q, :paren_close])
          @wff.class.should eql(Conjunction)
          @wff = Wff.from_symbols( [:paren_open, :tilde, :P, :ampersand, :Q, :paren_close])
          @wff.class.should eql(Conjunction)
          @wff = Wff.from_symbols( [:paren_open, :P, :ampersand, 
            :paren_open, :Q, :wedge, :Q2, :paren_close, :paren_close])
          @wff.class.should eql(Conjunction)
        end
      end
      describe "for a disjunction" do
        it "should return a disjunction" do
          @wff = Wff.from_symbols( [:paren_open, :P, :wedge, :Q, :paren_close])
          @wff.class.should eql(Disjunction)
          @wff = Wff.from_symbols( [:paren_open, :P, :wedge, :tilde, :Q, :paren_close])
          @wff.class.should eql(Disjunction)
          @wff = Wff.from_symbols( [:paren_open, :tilde, :P, :wedge, :Q, :paren_close])
          @wff.class.should eql(Disjunction)
          @wff = Wff.from_symbols( [:paren_open, :P, :wedge, 
            :paren_open, :Q, :double_arrow, :Q2, :paren_close, :paren_close])
          @wff.class.should eql(Disjunction)
        end
      end
      describe "for a conditional" do
        it "should return a conditional" do
          @wff = Wff.from_symbols( [:paren_open, :P, :arrow, :Q, :paren_close])
          @wff.class.should eql(Conditional)
          @wff = Wff.from_symbols( [:paren_open, :P, :arrow, :tilde, :Q, :paren_close])
          @wff.class.should eql(Conditional)
          @wff = Wff.from_symbols( [:paren_open, :tilde, :P, :arrow, :Q, :paren_close])
          @wff.class.should eql(Conditional)
          @wff = Wff.from_symbols( [:paren_open, :P, :arrow, 
            :paren_open, :Q, :double_arrow, :Q2, :paren_close, :paren_close])
          @wff.class.should eql(Conditional)
          @wff = Wff.from_symbols( [:paren_open, :P, :arrow, 
            :forall, :x, :P, :x, :paren_close])
          @wff.class.should eql(Conditional)
        end
      end
      describe "for a biconditional" do
        it "should return a biconditional" do
          @wff = Wff.from_symbols( [:paren_open, :P, :double_arrow, :Q, :paren_close])
          @wff.class.should eql(Biconditional)
          @wff = Wff.from_symbols( [:paren_open, :P, :double_arrow, :tilde, :Q, :paren_close])
          @wff.class.should eql(Biconditional)
          @wff = Wff.from_symbols( [:paren_open, :tilde, :P, :double_arrow, :Q, :paren_close])
          @wff.class.should eql(Biconditional)
          @wff = Wff.from_symbols( [:paren_open, :P, :double_arrow, 
            :paren_open, :Q, :double_arrow, :Q2, :paren_close, :paren_close])
          @wff.class.should eql(Biconditional)
          @wff = Wff.from_symbols( [:paren_open, :P, :double_arrow, 
            :forall, :x, :P, :x, :paren_close])
          @wff.class.should eql(Biconditional)
        end
      end
      describe "for an existential" do
        it "should return an existential" do
          @wff = Wff.from_symbols( [:exists, :x, :paren_open, :P, :x, :double_arrow, :Q, :paren_close])
          @wff.class.should eql(Existential)
          @wff = Wff.from_symbols( [:exists, :y, :forall, :x, :paren_open, :P, :x, :double_arrow, 
            :tilde, :Q, :y, :paren_close])
          @wff.class.should eql(Existential)
          @wff = Wff.from_symbols( [:exists, :x, :paren_open, :tilde, :P, :double_arrow, :Q, :x, :paren_close])
          @wff.class.should eql(Existential)
          @wff = Wff.from_symbols( [:exists, :x, :paren_open, :P, :double_arrow, 
            :paren_open, :tilde, :Q, :x, :double_arrow, :Q2, :paren_close, :paren_close])
          @wff.class.should eql(Existential)
          @wff = Wff.from_symbols( [:exists, :y, :paren_open, :P, :y, :double_arrow, 
            :forall, :x, :P, :x, :y, :paren_close])
          @wff.class.should eql(Existential)
        end
      end
      describe "for a universal" do
        it "should return a universal" do
          @wff = Wff.from_symbols( [:forall, :x, :paren_open, :P, :x, :double_arrow, :Q, :paren_close])
          @wff.class.should eql(Universal)
          @wff = Wff.from_symbols( [:forall, :y, :forall, :x, :paren_open, :P, :x, :double_arrow, 
            :tilde, :Q, :y, :paren_close])
          @wff.class.should eql(Universal)
          @wff = Wff.from_symbols( [:forall, :x, :paren_open, :tilde, :P, :double_arrow, :Q, :x, :paren_close])
          @wff.class.should eql(Universal)
          @wff = Wff.from_symbols( [:forall, :x, :paren_open, :P, :double_arrow, 
            :paren_open, :tilde, :Q, :x, :double_arrow, :Q2, :paren_close, :paren_close])
          @wff.class.should eql(Universal)
          @wff = Wff.from_symbols( [:forall, :y, :paren_open, :P, :y, :double_arrow, 
            :forall, :x, :P, :x, :y, :paren_close])
          @wff.class.should eql(Universal)
        end
      end
    end
  end

  describe ":from_string" do
    describe "with a valid string" do
      it "should create a Wff" do
        @wff = Wff.from_string('∀x∃y(Gx & ~Gy)')
        @wff.class.should eql(Universal)
      end
    end
  end

  # ------------------------------------
  # Decomposition of a WFF
  # ------------------------------------

  describe ":decompose" do
    describe "a valid" do
      describe "non-identity atomic sentence" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:P, :a, :b] )
          @decomp = @wff.decompose
          @decomp.step.should eql(nil)
          @decomp.branch_ct.should eql(0)
          @decomp.lines.should eql(0)
        end
      end
      describe "identity" do
        before(:each) do
          @tree = Tree.from_symbols([[:P, :a], [:Q, :b], [:exists, :x, :R, :x]])
          @leaf = @tree.leaves.first
          @wff1 = @leaf.elements.first.wff
        end
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:a, :eq, :b] )
          @new_wff = Wff.from_symbols( [:P, :b] )
          @decomp = @wff.decompose(@wff1, @new_wff)
          @decomp.step.should eql(:identity)
          @decomp.branch_ct.should eql(1)
          @decomp.lines.should eql(1)
          @wffs = @decomp.wffs          
          @wffs[0][0].symbols.should eql([:P, :b])
        end
      end

      describe "conjunction" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:paren_open, :P, :ampersand, :Q, :paren_close] )
          @decomp = @wff.decompose
          @decomp.class.should eql(Decomposition)
          @decomp.step.should eql(:ampersand)
          @decomp.branch_ct.should eql(1)
          @decomp.lines.should eql(2)
          @decomp.wffs[0][0].tokens[0].symbol.should eql(:P)
        end
      end
      describe "disjunction" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:paren_open, :P, :a1, :b2, :wedge, :Q, :paren_close] )
          @decomp = @wff.decompose
          @decomp.step.should eql(:wedge)
          @decomp.branch_ct.should eql(2)
          @decomp.lines.should eql(1)
          @decomp.wffs[0][0].tokens[0].symbol.should eql(:P)
        end
      end
      describe "conditional" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:paren_open, :P, :a1, :arrow, :Q, :a, :b, :paren_close] )
          @decomp = @wff.decompose
          @decomp.step.should eql(:arrow)
          @decomp.branch_ct.should eql(2)
          @decomp.lines.should eql(1)
          @decomp.wffs[0][0].tokens[0].symbol.should eql(:tilde)
        end
      end
      describe "biconditional" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:paren_open, :P, :a1, :double_arrow, :Q, :a, :b, :paren_close] )
          @decomp = @wff.decompose
          @decomp.step.should eql(:double_arrow)
          @decomp.branch_ct.should eql(2)
          @decomp.lines.should eql(2)
          @decomp.wffs[0][0].tokens[0].symbol.should eql(:P)
        end
      end
      describe "universal" do
        before(:each) do
          @tree = Tree.from_symbols([[:P, :a], [:Q, :b], [:forall, :x, :R, :x]])
          @leaf = @tree.leaves.first
        end
        it "should return a Decomposition instance" do
          @wff = @leaf.elements.last.wff
          @wff.class.should eql(Universal)
          @decomp = @wff.decompose( Name.new(:a) )
          @decomp.step.should eql(:forall)
          @decomp.branch_ct.should eql(1)
          @decomp.lines.should eql(1)
          @wffs = @decomp.wffs          
          @wffs[0][0].symbols.should eql([:R, :a])
        end
      end
      describe "existential" do
        before(:each) do
          @tree = Tree.from_symbols([[:P, :a], [:Q, :b], [:exists, :x, :R, :x]])
          @leaf = @tree.leaves.first
        end
        it "should return a Decomposition instance" do
          @wff = @leaf.elements.last.wff
          @wff.class.should eql(Existential)
          @decomp = @wff.decompose( @leaf.names + [@leaf.new_name] )
          @decomp.step.should eql(:exists)
          @decomp.branch_ct.should eql(3)
          @decomp.lines.should eql(1)
          @wffs = @decomp.wffs          
          @wffs[0][0].symbols.should eql([:R, :a])
          @wffs[1][0].symbols.should eql([:R, :b])
          @wffs[2][0].symbols.should eql([:R, :t1])
        end
      end
      describe "negated atomic sentence" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:a, :ne, :b] )
          @decomp = @wff.decompose
          @wff.literal?.should be_true
          @decomp.step.should eql(nil)
          @decomp.branch_ct.should eql(0)
          @decomp.lines.should eql(0)

          @wff = Wff.from_symbols( [:tilde, :P, :a, :b] )
          @decomp = @wff.decompose
          @wff.literal?.should be_true
          @decomp.step.should eql(nil)
          @decomp.branch_ct.should eql(0)          
          @decomp.lines.should eql(0)
        end
      end
      describe "negated negation" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:tilde, :a, :ne, :b] )
          @decomp = @wff.decompose
          @wff.literal?.should be_false
          @decomp.step.should eql(:double_neg)
          @decomp.branch_ct.should eql(1)
          @decomp.lines.should eql(1)

          @wff = Wff.from_symbols( [:tilde, :tilde, :P, :a, :b] )
          @decomp = @wff.decompose
          @wff.literal?.should be_false
          @decomp.step.should eql(:double_neg)
          @decomp.branch_ct.should eql(1)          
          @decomp.lines.should eql(1)
        end
      end
      describe "negated conjunction" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:tilde, :paren_open, :P, :ampersand, :Q, :paren_close])
          @decomp = @wff.decompose
          @decomp.step.should eql(:neg_ampersand)
          @decomp.branch_ct.should eql(2)
          @decomp.lines.should eql(1)
          @decomp.wffs[0][0].tokens[0].symbol.should eql(:tilde)
        end
      end
      describe "negated disjunction" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:tilde, :paren_open, :P, :wedge, :Q, :paren_close])
          @decomp = @wff.decompose
          @decomp.step.should eql(:neg_wedge)
          @decomp.branch_ct.should eql(1)
          @decomp.lines.should eql(2)
          @decomp.wffs[0][0].tokens[0].symbol.should eql(:tilde)
        end
      end
      describe "negated conditional" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:tilde, :paren_open, :P, :arrow, :Q, :paren_close])
          @decomp = @wff.decompose
          @decomp.step.should eql(:neg_arrow)
          @decomp.branch_ct.should eql(1)
          @decomp.lines.should eql(2)
          @decomp.wffs[0][0].tokens[0].symbol.should eql(:P)
        end
      end
      describe "negated biconditional" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( [:tilde, :paren_open, :P, :double_arrow, :Q, :paren_close])
          @decomp = @wff.decompose
          @decomp.step.should eql(:neg_double_arrow)
          @decomp.branch_ct.should eql(2)
          @decomp.lines.should eql(2)
          @decomp.wffs[0][0].tokens[0].symbol.should eql(:tilde)
        end
      end
      describe "negated universal" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( 
            [:tilde, :forall, :x, :paren_open, :P, :x, :arrow, :Q, :a, :paren_close] )
          @decomp = @wff.decompose
          @decomp.step.should eql(:neg_forall)
          @decomp.branch_ct.should eql(1)
          @decomp.lines.should eql(1)
          @wff1 = @decomp.wffs[0][0]
          @wff1.symbols[0].should eql(:exists)
          @wff1.symbols[2].should eql(:tilde)
        end
      end
      describe "negated existential" do
        it "should return a Decomposition instance" do
          @wff = Wff.from_symbols( 
            [:tilde, :exists, :x, :paren_open, :P, :x, :arrow, :Q, :a, :paren_close] )
          @decomp = @wff.decompose
          @decomp.step.should eql(:neg_exists)
          @decomp.branch_ct.should eql(1)
          @decomp.lines.should eql(1)
          @wff1 = @decomp.wffs[0][0]
          @wff1.symbols[0].should eql(:forall)
          @wff1.symbols[2].should eql(:tilde)
        end
      end
    end
    describe "with an incorrect sequence of tokens" do
      it "should return false" do
        Wff.existential?( Wff.get_tokens([:exists, :x, :Q])  ).should be_false
        Wff.existential?( Wff.get_tokens([:exists, :x, :y, :eq, :a]) ).should be_false
        Wff.existential?( Wff.get_tokens([:exists, :x, :R1, :y])  ).should be_false
      end
    end
  end

  describe ":negate" do
    it "should return a wff that is the negation of this wff" do
      @wff = Wff.from_symbols([:P])
      @wff.negate.symbols.should eql([:tilde, :P])
      @wff = Wff.from_symbols([:tilde, :P])
      @wff.negate.symbols.should eql([:P])
      @wff = Wff.from_symbols([:a, :eq, :b])
      @wff.negate.symbols.should eql([:a, :ne, :b])
      @wff = Wff.from_symbols([:a, :ne, :b])
      @wff.negate.symbols.should eql([:a, :eq, :b])
      @wff = Wff.from_symbols([:tilde, :forall, :x, :P, :x])
      @wff.negate.symbols.should eql([:forall, :x, :P, :x])
      @wff = Wff.from_symbols([:forall, :x, :P, :x])
      @wff.negate.symbols.should eql([:tilde, :forall, :x, :P, :x])
    end
  end

  describe ":literal?" do
    it "should be true if the wff is a literal" do
      @wff = Wff.from_symbols([:P])
      @wff.literal?.should be_true
      @wff = Wff.from_symbols([:tilde, :P])
      @wff.literal?.should be_true
      @wff = Wff.from_symbols([:a, :ne, :b])
      @wff.literal?.should be_true
    end
    it "should be false if the wff is not a literal" do
      @wff = Wff.from_symbols([:a, :eq, :b])
      @wff.literal?.should be_false
      @wff = Wff.from_symbols([:paren_open, :P, :wedge, :Q, :paren_close])
      @wff.literal?.should be_false
      @wff = Wff.from_symbols([:forall, :x, :F, :x])
      @wff.literal?.should be_false
    end
  end


end


# ------------------------------------
# Constructing a subtype directly
# ------------------------------------
describe "AtomicSentence" do
  describe "constructing an instance" do
    describe "from an array of tokens" do
      describe "with a correct set of tokens" do
        it "should construct an instance" do
          @atomic_sentence = AtomicSentence.new [:P3, :a23, :b1].map { |s| Token.from_sym(s) }
          @atomic_sentence.class.should eql(AtomicSentence)
        end
      end
    end
  end
end
