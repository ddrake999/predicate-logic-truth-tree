# encoding: UTF-8

require_relative './spec_helper'

describe "Token" do
  # ------------------------------------
  # Validate a string as a possible token
  # ------------------------------------
  describe ":valid" do
    describe "when the string is valid" do
      it "should be true" do
        %w{ P P1 Q2 A123 a b32 d9 u x42 z12 }.each do |s|
          Token.valid?(s).should eql true
        end
        [ '(', '{', '[', ')', '}', ']', '∀', '∃', '~', '∨', '&','→', '↔', '=' ].each do |s|
          Token.valid?(s).should eql true
        end
      end
    end
    describe "when the string is not a valid token" do
      it "should raise an error" do
        %w{ $ % ^ 42 }.each do |s|
          expect { Token.from_string(s) }.to raise_error
        end
      end
    end
  end

  # ------------------------------------
  # Construct a token from a string
  # ------------------------------------  
  describe ":from_string" do
    describe "where the string is a valid token" do
      it "should create a token" do
        %w{ P P1 Q2 A123 }.each do |s|
          @token = Token.from_string(s)
          @token.symbol.should eql(s.to_sym)
          @token.to_s.should eql(s)
          @token.class.should eql(PredicateLetter)
        end

        %w{ a b32 d9 }.each do |s|
          @token = Token.from_string(s)
          @token.symbol.should eql(s.to_sym)
          @token.to_s.should eql(s)
          @token.class.should eql(Name)
        end

        %w{ u x42 z12 }.each do |s|
          @token = Token.from_string(s)
          @token.symbol.should eql(s.to_sym)
          @token.to_s.should eql(s)
          @token.class.should eql(Variable)
        end

        ['(', '{', '[' ].each do |s|
          @token = Token.from_string(s)
          @token.symbol.should eql(:paren_open)
          @token.to_s.should eql('(')
          @token.class.should eql(Paren)
        end

        [')', '}', ']' ].each do |s|
          @token = Token.from_string(s)
          @token.symbol.should eql(:paren_close)
          @token.to_s.should eql(')')
          @token.class.should eql(Paren)
        end

        @token = Token.from_string('∀')
        @token.symbol.should eql(:forall)
        @token.to_s.should eql('∀')
        @token.class.should eql(Quantifier)
        @token = Token.from_string('∃')
        @token.symbol.should eql(:exists)
        @token.to_s.should eql('∃')
        @token.class.should eql(Quantifier)

        @token = Token.from_string('~')
        @token.symbol.should eql(:tilde)
        @token.to_s.should eql('~')
        @token.class.should eql(Connective)
        @token = Token.from_string('∨')
        @token.symbol.should eql(:wedge)
        @token.to_s.should eql('∨')
        @token.class.should eql(Connective)
        @token = Token.from_string('&')
        @token.symbol.should eql(:ampersand)
        @token.to_s.should eql('&')
        @token.class.should eql(Connective)
        @token = Token.from_string('→')
        @token.symbol.should eql(:arrow)
        @token.to_s.should eql('→')
        @token.class.should eql(Connective)
        @token = Token.from_string('↔')
        @token.symbol.should eql(:double_arrow)
        @token.to_s.should eql('↔')
        @token.class.should eql(Connective)

        @token = Token.from_string('=')
        @token.symbol.should eql(:eq)
        @token.to_s.should eql('=')
        @token.class.should eql(IdentitySymbol)
        @token = Token.from_string('≠')
        @token.symbol.should eql(:ne)
        @token.to_s.should eql('≠')
        @token.class.should eql(IdentitySymbol)
      end
    end

    describe "where the string is invalid" do
      it "should raise an error" do
        %w{ $ % ^ 42 }.each do |s|
          expect { Token.from_string(s) }.to raise_error
        end
      end
    end

  end

  describe ":to_s" do
    it "should get the string for a token" do
      @syms = [:forall, :x1, :exists, :y, :paren_open, :A, :ampersand, :B, :x, :y, :paren_close]
      @syms.map { |sym| Token.new(sym).to_s }.join('').should == '∀x1∃y(A&Bxy)'
    end
  end

  # ------------------------------------------------------
  # Extract a single token from the beginning of a string
  # ------------------------------------------------------
  describe ":extract" do
    describe "when the string begins with a valid token" do
      it "should return a token and a trimmed string with the token removed" do
        %w{ A1b &x  }.each do |s|
          @token, rest = Token.extract(s)
          @token.class.superclass.should eql(Token)
          rest.size.should be < s.size
        end
        @token, rest = Token.extract(' A1b( ')
        @token.symbol.should eql(Token.from_string('A1').symbol)
        @token.class.should eql(PredicateLetter)
        rest.should eql('b(')
      end
    end

    describe "when the string doesn't begin with a valid token" do
      it "should raise an error" do
        %w{ $A %12 ^32 42 }.each do |s|
          expect { Token.extract(s) }.to raise_error
        end
      end
    end

  end


  # ------------------------------------------------------
  # Extract all tokens from a string
  # ------------------------------------------------------
  describe ":tokenize" do
    describe "when the string is a sequence of valid tokens with optional whitespace" do
      it "should return an array of tokens" do
        @tokens = Token.tokenize(' ∀ x1∃y(A & Bxy )')
        # @tokens = Token.tokenize(' ∀ x1')
        @tokens.map { |t| t.symbol }.should eql [:forall, 
          :x1, :exists, :y, :paren_open, :A, :ampersand, :B, :x, :y, :paren_close]
        @tokens.map { |t| t.class }.should eql [Quantifier, 
          Variable, Quantifier, Variable, Paren, PredicateLetter, 
          Connective, PredicateLetter, Variable, Variable, Paren]
      end
    end

    describe "when the string contains some invalid tokens" do
      it "should raise an error" do
        expect { Token.tokenize('Pxy & B^x9 ')}.to raise_error(ArgumentError)
      end
    end

  end

end


