# encoding: UTF-8

require_relative './spec_helper'

describe "Truth Tree" do
  describe "with a formula that requires ED2" do
    it "should create a tree with a complete open branch" do
      @tree = Tree.from_lines(['∀x∃y Pxy'])
      @fs = @tree.formula_selector
      @elem, @args = @fs.get_next
      @elem.wff.class.should eql(Universal)
      @args[:name].symbol.should eql(:t1)
      @tree.decompose(@elem, @args)
      @leaf1 = @tree.leaves.first
      @leaf1.all_elements.count.should eql(2)
      @leaf1.all_elements[1].wff.class.should eql(Existential)

      @elem, @args = @fs.get_next
      @elem.wff.class.should eql(Existential)
      @tree.decompose(@elem, @args)
      @tree.leaves.count.should eql(2)
      @tree.leaves.first.all_elements.last.wff.should == Wff.from_symbols([:P, :t1, :t1])
      @tree.leaves.last.all_elements.last.wff.should == Wff.from_symbols([:P, :t1, :t2])
      @tree.leaves.first.open_complete?.should be_true
      # @tree.display
    end
  end

  describe ":process_until_closed_or_model_found" do
    describe "with a tree with a simple branching formula and literals" do
      describe "when the tree has no model" do
        it "should select formulas and decompose them into the tree until all leaves are closed" do
          @tree = Tree.from_lines([
            'P',
            '(P → Q)',
            '~Q',
          ])
          @tree.process_until_closed_or_model_found
          @tree.leaves.count.should eql(2)
          @tree.closed_leaves.count.should eql(2)
        end
      end
      describe "when the tree has one or more models" do
        it "should select formulas and decompose them into the tree until a model is found" do
          @tree = Tree.from_lines([
            'P',
            '(P → Q)',
            'Q',
          ])
          @tree.process_until_closed_or_model_found
          @tree.leaves.count.should eql(2)
          @tree.closed_leaves.count.should eql(1)
          @tree.open_complete_leaves.count.should eql(1)
        end
      end
    end
    describe "with a tree with a universal" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines(['∀x∃y Pxy'])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
      end
    end
    describe "with a tree some identities" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines [
          'a=b',
          'b=c',
          'a≠c',
        ]
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
      end
    end
    describe "with a made up set of formulas" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines [
          '(~Pa ∨ Q)',
          '(Q → R)',
          '∀x(Px & R)'
        ]
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end


    describe "Allen & Hand 1.5.4 S66 L→R" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(P↔Q)',
          '~((P∨Q) → (P&Q))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(6)
        @tree.closed_leaves.count.should eql(6)
        @tree.open_complete_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 1.5.4 S81" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(P∨(R∨Q))',
          '((R→S) & (Q→T))',
          '((S∨T)→(P∨Q))',
          '~P',
          '~Q',
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(6)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 1.5.4 S85" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '((P&Q) → ((R∨S) & ~(R&S)))',
          '((R&Q)→S)',
          '(S→ (((R&Q) ∨ (~R&~Q)) ∨ ~P))',
          '~(P → ~Q)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 40
        @tree.leaves.count.should eql(17)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    # Exercise 3.3.2
    describe "Allen & Hand S87" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines %w{ ∃x(Gx&~Fx) ∀x(Gx→Hx) ~∃x(Hx&~Fx) }
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        @tree.open_complete_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S88" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines %w{ ∃x(Gx&Fx) ∀x(Fx→~Hx) ~∃x~Hx }
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(0)
        @tree.open_complete_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S89" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines %w{ ∀x(Gx→~Fx) ∀x(~Fx→~Hx) ~∀x(Gx→~Hx) }
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        @tree.open_complete_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S90" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines [
          '∃x(Fx & Ga)',
          '∀x(Fx→Hx)',
          '~(Ga & ∃x(Fx & Hx))',
        ]
        @tree.process_until_closed_or_model_found(max_lines: 30)
        @tree.leaves.count.should eql(12)
        @tree.pending_leaves.count.should eql(0)
        @tree.open_complete_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S91" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx→∃y(Fy&Hy))',
          '~(∀x~Fx→~∃zGz)'
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S91" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx→∃y(Fy&Hy))',
          '~(∀x~Fx→~∃zGz)'
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S91" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx→∃y(Fy&Hy))',
          '~(∀x~Fx→~∃zGz)'
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S91" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx→∃y(Fy&Hy))',
          '~(∀x~Fx→~∃zGz)'
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S91" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx→∃y(Fy&Hy))',
          '~(∀x~Fx→~∃zGz)'
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S91" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx→∃y(Fy&Hy))',
          '~(∀x~Fx→~∃zGz)'
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S91" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx→∃y(Fy&Hy))',
          '~(∀x~Fx→~∃zGz)'
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S92" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx → (Hx & Jx))',
          '∀x((Fx ∨ ~Jx) → Gx)',
          '~∀x(Fx → Hx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S93" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx & (Kx↔Hx))',
          '~∃x(Fx & Gx)',
          '~∀x~(Fx & Hx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S94" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx → Hx)',
          '∃x((Fx & Gx) & Mx)',
          '~∃x(Fx & (Hx & Mx))',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(4)
        @tree.closed_leaves.count.should eql(4)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S95" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(~Gx ∨ ~Hx)',
          '∀x((Jx → Fx) → Hx)',
          '∃x(Fx & Gx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S96" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '~∃x(~Gx & Hx)',
          '∀x(Fx → ~Hx)',
          '~∀x((Fx ∨ ~Gx) → ~Hx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(4)
        @tree.closed_leaves.count.should eql(4)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S97" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x~(Gx & Hx)',
          '∃x(Fx & Gx)',
          '~∃x(Fx & ~Hx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S98" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∃x(Fx & ~Hx)',
          '~∃x(Fx & ~Gx)',
          '∀x(Gx → Hx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S99" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Hx → (Hx & Gx))',
          '∃x(~Gx & Fx)',
          '~∃x(Fx & ~Hx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S100" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Hx → ~Gx)',
          '~∃x(Fx & ~Gx)',
          '~∀x~(Fx & Hx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 20)
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S101" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Fx ↔ Gx)',
          '~(∀xFx ↔ ∀xGx)',
       ])
        @tree.process_until_closed_or_model_found(max_lines: 30)
        @tree.leaves.count.should eql(8)
        @tree.closed_leaves.count.should eql(8)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S102" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(∃xFx→∀y(Gy→Hy))',
          '(∃xJx → ∃xGx)',
          '~(∃x(Fx&Jx)→∃zHz)'
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(6)
        @tree.closed_leaves.count.should eql(6)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end


    describe "Allen & Hand S103" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(∃xFx ∨∃xGx)',
          '∀x(Fx → Gx)',
          '~∃xGx',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(6)
        @tree.closed_leaves.count.should eql(6)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S104" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Fx → ~Gx)',
          '∃x(Fx & Gx)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(2)
        @tree.closed_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S105" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x((Fx ∨Hx) → (Gx & Kx))',
          '~∀x(Kx & Gx)',
          '~∃x~Hx',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.closed_leaves.count.should eql(4)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S106" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x((Fx & Gx) → Hx)',
          '(Ga & ∀xFx)',
          '~(Fa & Ha)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.closed_leaves.count.should eql(4)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S107" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Fx ↔ ∀yGy)',
          '~(∀xFx ∨ ∀x~Fx)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(6)
        @tree.closed_leaves.count.should eql(6)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    # SLOW - 0.1s
    describe "Allen & Hand S108" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀y(Fa→(∃xGx → Gy))',
          '∀x(Gx → Hx)',
          '∀x(~Jx → ~Hx)',
          '~(∃x~Jx → (~Fa ∨ ∀x~Gx))',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(18)
        @tree.closed_leaves.count.should eql(18)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S109" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Dx → Fx)',
          '~∀z(Dz → (∀y(Fy → Gy) → Gz))',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S110" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(∃xFx ↔ ∀y((Fy ∨ Gy) → Hy))',
          '∃xHx',
          '~∀z~Fz',
          '~∃x(Fx & Hx)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(7)
        @tree.closed_leaves.count.should eql(7)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S111" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀xFx',
          '~(~∃xGx ↔ ~(∃x(Fx & Gx) & ∀y(Gy → Fy)))',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(12)
        @tree.closed_leaves.count.should eql(12)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S112" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(∃yFyx → ∀zFxz)',
          '~∀y∀x(Fyx → Fxy)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(5)
        @tree.closed_leaves.count.should eql(5)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S113" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∃x(Fx & ∀yGxy)',
          '∀x∀y(Gxy → Gyx)',
          '~∃x(Fx & ∀yGyx)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(5)
        @tree.closed_leaves.count.should eql(5)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S114" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∃x~∀y(Gxy → Gyx)',
          '~∃x∃y(Gxy & ~Gyx)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S115" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Gx → ∀y(Fy → Hxy))',
          '∃x(Fx & ∀z~Hxz)',
          '∀xGx',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S116" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x∀y(Fxy → Gxy)',
          '~∀x(Fxx → ∃y(Gxy & Fyx))',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S117" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x∀y(Fxy → ~Fyx)',
          '∃xFxx',
       ])
        @tree.process_until_closed_or_model_found
        # @tree.display
        @tree.leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
      end
    end
    describe "Allen & Hand S118" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x∃y(Fxy & ~Fyx)',
          '~∃x~∀yFxy',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(2)
        @tree.closed_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S119" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀y(∃x~Fxy → ~Fyy)',
          '~∀x(Fxx → ∀yFyx)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S120" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(∃xFxx → ∀x∀yFxy)',
          '~∀x(Fxx → ∀yFxy)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S121" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          'a=b',
          'b≠a',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S122" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(a=b & b=c)',
          'a≠c',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S123" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          'a=b',
          'b≠c',
          'a=c',
       ])
        @wff = @tree.leaves[0].elements[1].wff
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S124" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(Fa & ∀x(Fx → x=a))',
          '∃x(Fx & Gx)',
          '~Ga',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.closed_leaves.count.should eql(4)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S125" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '(∀x x=x → ∃xFx)',
          '∀x(~Fx ∨ Gx)',
          '~∃x(Fx & Gx)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(15)
        @tree.closed_leaves.count.should eql(15)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S126" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Fx → Gx)',
          '∀x(Gx → Hx)',
          '(Fa & ~Hb)',
          '~a≠b',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(5)
        @tree.closed_leaves.count.should eql(5)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S127" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∃x((Fx & ∀y(Fy → y=x)) & Gx)',
          '~Ga',
          'Fa',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.closed_leaves.count.should eql(4)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S128" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∃x∀y((~Fxy → x=y) & Gx)',
          '~∀x(~Gx → ∃y(y≠x & Fyx))',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(7)
        @tree.closed_leaves.count.should eql(7)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand S129" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∃x(Px & (∀y(Py → y=x) & Qx))',
          '∃x~(~Px ∨ ~Fx)',
          '~∃x(Fx & Qx)',
       ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(6)
        @tree.closed_leaves.count.should eql(6)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    # SLOW 0.1s
    describe "Allen & Hand S130" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x∃yGyx',
          '∀x∀y(Gxy → ~Gyx)',
          '∃y∀x(x≠y → Gyx)',
       ])
        @tree.process_until_closed_or_model_found
        # @tree.display
        @tree.leaves.count.should eql(10)
        @tree.closed_leaves.count.should eql(10)
        @tree.pending_leaves.count.should eql(0)
      end
    end






    describe "Allen & Hand 4.2 i" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∀xFx → ∀xGx)',
          '~∀x(Fx → Gx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 ii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∃xFx → ∃xGx)',
          '~∀x(Fx → Gx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 iii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∃xFx & ∃xGx)',
          '~∃x(Fx & Gx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 iv" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(Fx ∨ Gx)',
          '~(∀xFx ∨ ∀xGx)',
        ])
        @tree.process_until_closed_or_model_found
        # @tree.display
        @tree.leaves.count.should eql(10)
        @tree.open_complete_leaves.count.should eql(6)
        @tree.pending_leaves.count.should eql(0)
      end
    end
    describe "Allen & Hand 4.2 v" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(Fx→Gx)',
          '~(∃xFx → ∃xGx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 vi" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(Fx → Gx)',
          '~(∀xFx → ∀xGx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 vii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∀xFx ↔ ∀xGx)',
          '~∀x(Fx ↔ Gx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(11)
        @tree.open_complete_leaves.count.should eql(6)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 viii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∃xFx ↔ ∃xGx)',
          '~∀x(Fx ↔ Gx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(11)
        @tree.open_complete_leaves.count.should eql(6)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 ix" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∀xFx ↔ P)',
          '~∀x(Fx ↔ P)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(5)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 x" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∃xFx ↔ P)',
          '~∀x(Fx ↔ P)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(5)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xi" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(Fx ↔ P)',
          '~(∃xFx ↔ P)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(5)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(Fx ↔ P)',
          '~(∀xFx ↔ P)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(5)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xiii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x(Fx → Gx)',
          '∀x(Gx → Hx)',
          '~∀x(Hx → Fx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.open_complete_leaves.count.should eql(3)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xiv" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x(Fx → ~Hx)',
          '∀x(Hx → ~Gx)',
          '~∃x(Fx & Gx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(8)
        @tree.open_complete_leaves.count.should eql(8)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xv" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∃xFx↔∀xGx)',
          '~∀x(Fx → Hx)',
          '~(∃xHx → ∃x~Gx)',
        ])
        @tree.process_until_closed_or_model_found
        # @tree.display
        @tree.leaves.count.should eql(7)
        @tree.open_complete_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(1)
      end
    end
    describe "Allen & Hand 4.2 xvi" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x(Gx ∨ ~Hx)',
          '∃x(Gx & Fx)',
          '~∃x~Hx',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xvii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x((Fx & Gx) → Hx)',
          '∃x(Fx & Hx)',
          '~∃xGx',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xviii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃xFx',
          '∃xGx',
          '∃xHx',
          '~∀x((Fx ∨ Gx) → Hx)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(16)
        @tree.open_complete_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(9)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xix" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '~∀xFx',
          '~∀x~Fx',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.2 xx" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(Fx → ∃yGy)',
          '~(∃xFx → ∃yGy)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(2)
        # @tree.display
      end
    end






    describe "Allen & Hand 4.3.1 i" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_symbols([
          [:exists, :x, :F, :x, :x],
          [:tilde, :forall, :x, :forall, :y, :F, :y, :x],
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.3.1 ii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀y∃xFxy',
          '~∃xFxx',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.3.1 iii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x∃yFxy',
          '~∃x∀yFxy',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(14)
        @tree.open_complete_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(7)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.3.1 iv" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x∃y~Fxy',
          '∀x∀y(Gxy → ~Fxy)',
          '~∀x∃y~Gxy',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.3.1 v" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x(Fx → ∃yGxy)',
          '~∀x∀y(Fx ∨ ~Gxy)',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(4)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(3)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.3.1 vii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x~∀yTxy',
          '~∀x~∃yTxy',
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(7)
        @tree.open_complete_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(3)
        # @tree.display
      end
    end

    # SLOW 
    # Interesting! 112 nodes!?  Takes a long time - 3.2s selecting, .184s processing
    describe "Allen & Hand 4.3.1 viii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x∃y∃z((Fxy&Fyz) & ~(Fxz ∨Fyx))',
          '~(∀x∃yFyx → ∀x~Fxx)',
        ])
        @tree.process_until_closed_or_model_found
        # @tree.display
        @tree.leaves.count.should eql(48)
        @tree.open_complete_leaves.count.should eql(6)
        @tree.pending_leaves.count.should eql(26)
      end
    end
    # FAILURE
    # The algorithm detected that the formula in the book is not a WFF (missing ∃x in conclusion)!
    # this is a modified version, but it doesn't converge...
    describe "Allen & Hand 4.3.1 ix" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x∃yFxy',
          '∃x~∀yGyx',
          '(∃x∃yFxy ↔ ∃x∃y(Gyx & ~Gxy))',
          '~∃x∃y(Gxy ∨ Gyx)'
        ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(6)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(4)
        # @tree.display
      end
    end
    # SLOW 0.12s
    describe "Allen & Hand 4.3.1 x" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∃x∃yFxy ↔ ~∃xGxx)',
          '∀y∃xGyx',
          '~∀x~Fxx'
        ])
        @tree.process_until_closed_or_model_found
        @tree.leaves.count.should eql(17)
        @tree.open_complete_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(10)
        # @tree.display
      end
    end





    describe "Allen & Hand 4.3.2 i" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x(Fx→~Gx)',
          '∃x(Gx & ~Fx)',
          '~(~∃xFx ∨ ∀x~Gx)'
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(6)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(2)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 ii" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x(Fx→~Gx)',
          '∃x(Gx & Fx)',
          '~(~∃xFx ∨ ∀x~Gx)'
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 iii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∃x(Fx→Gx) → ∃x(Fx & ~Hx))',
          '~(∃x(Fx&Gx) → ~∀x(Gx→Hx))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 30
        @tree.leaves.count.should eql(5)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 iv" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '(∃x(Fx→Gx) → ∃x(Fx & ~Hx))',
          '~(∃x(Fx&Gx) → ∀x(Gx→Hx))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 30
        @tree.leaves.count.should eql(6)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(4)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 v" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(Fx ∨ P)',
          '(P↔~∃xGx)',
          '~∃x(Fx & Gx)',
          '∀x(Hx → (~Fx & ~Gx))',
          '~(P ∨ ∀x(Hx → P))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(15)
        @tree.open_complete_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 vi" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x((Fx & Gx) → Hx)',
          '∃xFx',
          '~∃x(Gx → Hx)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 vii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x((Fx & ~Gx) → Hx)',
          '∃xFx',
          '~∃x(Hx → ~Gx)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(1)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 viii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '~((∀xFx ∨ ∃x~Gx) → ~∃x(Fx ∨ Gx))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(4)
        @tree.open_complete_leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(2)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 ix" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '~((∀xFx ∨ ∃xGx) → ∃x(Fx ∨ Gx))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 x" do
      it "should select formulas and decompose them into the tree until the tree is complete" do
        @tree = Tree.from_lines([
          '∃x(Fx ∨ Gx)',
          '(∃xFx → ∀xHx)',
          '(∃xGx → ~∃xHx)',
          '(∃xHx & ∃x~Hx)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(12)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 xi" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(Fx ∨ Gx)',
          '(∃xFx → ∀xHx)',
          '(∃xGx → ~∃xHx)',
          '(∀xHx ∨ ∀x~Hx)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(9)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(2)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 xii" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∃x(Bx & ∀y(Sxy ↔ ~Syy))',
        ])
        @tree.process_until_closed_or_model_found max_lines: 50
        # @tree.display
        @tree.leaves.count.should eql(1)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
      end
    end

    describe "Allen & Hand 4.3.2 xiii" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '~(∀x∀y(Fxy → Fyx) ↔ ∀x∀y(Fxy ↔ Fyx))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        # @tree.display
        @tree.leaves.count.should eql(16)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
      end
    end

    describe "Allen & Hand 4.3.2 xiv" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '~(∀x∀y(Fxy → Fxy) ↔ ∀x∀y(Fxy ↔ Fyx))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        # @tree.display
        @tree.leaves.count.should eql(19)
        @tree.open_complete_leaves.count.should eql(8)
        @tree.pending_leaves.count.should eql(0)
      end
    end

    describe "Allen & Hand 4.3.2 xv" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x∀y∀z((Rxy & Rxz) → ~Ryz)',
          '∀xRxx',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        # @tree.display
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
      end
    end

    describe "Allen & Hand 4.3.2 xvi" do
      it "should select formulas and decompose them into the tree until the tree is complete" do
        @tree = Tree.from_lines([
          '∀x∀y∀z((Rxy & Ryz) → ~Rxz)',
          '~∀x~Rxx',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 xvii" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∀x∀y∀z((Fxy&Fyz)→Fxz)',
          '∀x∀y(Fxy→Fyx)',
          '~(∀x∃yFxy → ∀xFxx)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(8)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    # FAILURE After 70 lines, things keep getting worse...
    # SLOW ~.6s
    describe "Allen & Hand 4.3.2 xviii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x∀y∀z((Fxy&Fyz)→Fxz)',
          '∀x∃y(Fxy→Fyx)',
          '~(∀x∃yFxy → ∀xFxx)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(12)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(2)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 xix" do
      it "should select formulas and decompose them into the tree until the tree is closed" do
        @tree = Tree.from_lines([
          '∃x∀y~Fxy',
          '~∃x∀y∀z(Fxz → Fzy)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(5)
        @tree.open_complete_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    describe "Allen & Hand 4.3.2 xx" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x∀yFxy',
          '~∃x~∀y∀z(Fxz → Fzy)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 50
        @tree.leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end





    # todo: how should the model be represented?
    describe "Allen & Hand 4.4 i" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          'a=b',
          'c=d',
          'a≠c'
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(1)
        @tree.open_complete_leaves.count.should eql(1)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    # In this case, we have a completed open branch after adding the set members, so no decomposition 
    # is required.  Todo: improve display of model for names
    describe "Allen & Hand 4.4 ii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          'Fa',
          'a≠b',
          'Fb'
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(0)
        @tree.pending_leaves.count.should eql(0)
        # @tree.display
      end
    end

    # SLOW - 0.26s
    # This one simplifies to two existentials: ∃y a=y  and ∃x a≠x.  A human would choose to decompose those
    # to obtain a model a≠b.   I think the formula selector should be changed so that 
    # we don't decompose a universal until all pending existentials have been decomposed.
    describe "Allen & Hand 4.4 iii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x∃y x=y',
          '~∃y∀x y=x',
       ])
        # @tree.process_until_closed_or_model_found max_lines: 11
        @tree.process_until_closed_or_model_found
        # @tree.display
        # @node = @tree.pending_leaves[0]
        # @pels = @node.pending_elements
        @tree.open_complete_leaves.count.should > 0
      end
    end

    describe "Allen & Hand 4.4 iv" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(x≠a → Fx)',
          'a=b',
          '~Fb',
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(5)
        @tree.pending_leaves.count.should eql(3)
        @tree.closed_leaves.count.should eql(1)
        @tree.open_complete_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.4 v" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x∃y((Fx & Fy) & x≠y)',
          '~∀xFx',
       ])
        @tree.process_until_closed_or_model_found max_lines: 30
        # @tree.display
        @tree.leaves.count.should eql(5)
        @tree.pending_leaves.count.should eql(0)
        @tree.closed_leaves.count.should eql(4)
        @tree.open_complete_leaves.count.should eql(1)
      end
    end
    describe "Allen & Hand 4.4 vi" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x∀y((Fx & Gy) → x=y)',
          '∃x(Fx & Gx)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(0)
        @tree.open_complete_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.4 vii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x(x≠a → (Fx ∨ Gx))',
          '∃x((Fx ∨ Gx) → x≠a)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(9)
        @tree.pending_leaves.count.should eql(6)
        @tree.closed_leaves.count.should eql(2)
        @tree.open_complete_leaves.count.should eql(1)
        # @tree.display
      end
    end
    describe "Allen & Hand 4.4 viii" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x∀y(Fxy → y=x)',
          '~∃xFxx',
       ])
        @tree.process_until_closed_or_model_found max_lines: 20
        @tree.leaves.count.should eql(2)
        @tree.pending_leaves.count.should eql(0)
        @tree.closed_leaves.count.should eql(0)
        @tree.open_complete_leaves.count.should eql(2)
        # @tree.display
      end
    end
    # Nice! I couldn't have done it better myself -- even though it takes 38 lines!
    describe "Allen & Hand 4.4 ix" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∃x∀y(Fxy ↔ x≠y)',
          '~∀x∀y∀z((Fxy & Fxz) → y=z)',
       ])
        @tree.process_until_closed_or_model_found max_lines: 40
        @tree.leaves.count.should eql(9)
        @tree.pending_leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(7)
        @tree.open_complete_leaves.count.should eql(1)
        # @tree.display
      end
    end
    # SLOW - ~ 1s
    # It seems this could be done in fewer steps by having a rule that gives more weight to nodes with
    # fewer names...  U: {a} F: {}
    describe "Allen & Hand 4.4 x" do
      it "should select formulas and decompose them into the tree until a model is found" do
        @tree = Tree.from_lines([
          '∀x∀y((Fx & Fy) → x≠y)',
          '~∀x∀y∀z(((Fx & Fy) & Fz) & ((x≠y & y≠z) & x≠z))',
       ])
        @tree.process_until_closed_or_model_found max_lines: 30
        # @tree.display
        @tree.leaves.count.should eql(8)
        @tree.pending_leaves.count.should eql(4)
        @tree.closed_leaves.count.should eql(3)
        @tree.open_complete_leaves.count.should eql(1)
      end
    end
    # FAILURE (but expected...)
    # SLOW - 0.1s    
    # Is there a way to produce a model of some sort by assuming the names to be natural numbers?
    describe "Allen & Hand 4.5.2 i (infinite countermodel)" do
      it "should select formulas and decompose them into the tree until the maximumum lines is reached" do
        @tree = Tree.from_lines([
          '∀x∀y∀z((Fxy & Fyz) → Fxz)',
          '∀x∃yFxy',
          '~∃xFxx'
       ])
        @tree.process_until_closed_or_model_found max_lines: 30
        @tree.leaves.count.should eql(7)
        @tree.pending_leaves.count.should eql(1)
        @tree.closed_leaves.count.should eql(6)
        # @tree.display
      end
    end



  end


end