# encoding: UTF-8

require_relative './spec_helper'

describe "Diagram" do
  describe ":initialize" do
    before(:each) do
      @tree = Tree.from_lines( %w{Pa (Q→~Pa) ~∀x(Px→Q)} )
      @diagram = Diagram.new(@tree)
    end
    it "should have a tree" do
      @diagram.tree.class.should eql(Tree)
    end
  end

  describe ":build" do
    describe "with a non-decomposed tree" do
      before(:each) do
        @tree = Tree.from_lines( %w{ Pa (Q→~Pa) ~∀x(Px→Q) } )
        @diagram = Diagram.new(@tree)
      end
      it "should have 3 lines" do
        @diagram.build
        @diagram.lines.count.should eql(6)
      end
    end    
    describe "with a non-branching tree" do
      before(:each) do
        @tree = Tree.from_lines( %w{ Pa ~(Q→~Pa) ~∀x(Px→Q) } )
        @selector = FormulaSelector.new(@tree)
        @element, @args = @selector.get_next
        @tree.decompose(@element, @args)
        @diagram = Diagram.new(@tree)
      end
      it "should get a line of the tree" do
        @tree.last_line.should eql(5)
        @diagram.build
        @diagram.lines.count.should eql(10)
        @diagram.lines[0].should eql("1.       1: Pa ✓                                  SM")
        @diagram.lines[2].should eql("2.       1: ~(Q → ~Pa) ✓                          SM")
        @diagram.lines[4].should eql("3.       1: ~∀x (Px → Q)                          SM")
        @diagram.lines[6].should eql("4.       2(1): Q ✓                                2 ~→D")
        @diagram.lines[8].should eql("5.       2(1): Pa ✓                               2 ~→D")
      end  
    end  
    describe "with a branching tree" do
      before(:each) do
        @tree = Tree.from_lines( %w{ Pa (Q→~Pa) ∀x(Px→Q) })
        @selector = FormulaSelector.new(@tree)
        @element, @args = @selector.get_next
        @tree.decompose(@element, @args)
        @diagram = Diagram.new(@tree)
        # @tree.display
      end
      it "should get a line of the tree" do
        @tree.leaves.count.should eql(2)
        @diagram.build
        @diagram.lines.count.should eql(8)
        @diagram.lines[0].should eql("1.       1: Pa ✓                                  SM")
        @diagram.lines[2].should eql("2.       1: (Q → ~Pa) ✓                           SM")
        @diagram.lines[4].should eql("3.       1: ∀x (Px → Q)                           SM")
        @diagram.lines[6].should eql("4.       2(1): ~Q ✓      3(1): ~Pa (closes) ✓     2 →D")
        @diagram.lines[7].should eql("                                  X             ")
        @tree.last_line.should eql(4)
      end  
    end  
  end


end

