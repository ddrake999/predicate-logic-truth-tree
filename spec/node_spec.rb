# encoding: UTF-8

require_relative './spec_helper'

describe "Node" do
  describe ":initialize" do
    describe "for the root node" do
      before(:each) do
        @tree = Tree.new
        @node = Node.new(@tree)
      end

      it "should have an empty array of elements" do
        @node.elements.should == []
      end

      its "parent node should be nil" do
        @node.parent.should be_nil
      end

      its "state should be pending" do
        @node.state.should eql(:pending)
      end

    end

  end




end
